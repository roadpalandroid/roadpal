package com.rootcave.roadpal.settings;

import android.content.SharedPreferences;

/**
 * Created by Lenovo T420 on 22.12.2015..
 */
public interface ISettingsPresenter {
    void processSharedPreferences(SharedPreferences preferences,String key );


}
