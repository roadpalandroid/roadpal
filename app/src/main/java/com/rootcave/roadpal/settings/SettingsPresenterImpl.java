package com.rootcave.roadpal.settings;

import android.content.SharedPreferences;

/**
 * Created by Lenovo T420 on 22.12.2015..
 */
public class SettingsPresenterImpl implements ISettingsPresenter, OnProcessPreferencesCompleteListener {

    private ISettingsView mSettingsView;
    private ISettingsInteractor mInteractor;

    public SettingsPresenterImpl(ISettingsView view) {
        mSettingsView = view;
        mInteractor = new SettingsInteractorImpl(this);

    }

    //method from ISettingsPresenter
    @Override
    public void processSharedPreferences(SharedPreferences preferences, String key) {
        mInteractor.processSharedPrefs(preferences,key);

    }


    //methods from OnProcessPreferencesCompleteListener
    @Override
    public void onProcessPreferenceSucess() {

    }

    @Override
    public void onProcessPreferenceFailure() {

    }
}
