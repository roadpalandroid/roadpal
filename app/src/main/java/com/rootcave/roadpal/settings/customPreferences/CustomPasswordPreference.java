package com.rootcave.roadpal.settings.customPreferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.rootcave.roadpal.Login.LoginActivity;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by Lenovo T420 on 21.12.2015..
 */
public class CustomPasswordPreference extends DialogPreference implements View.OnClickListener, TextWatcher {
    // @Bind(R.id.password_edit_text)MaterialEditText passwordEditText;
    // @Bind(R.id.password_again_edit_text)MaterialEditText passwordAgainEditText;

    private MaterialEditText passwordEditText;
    private MaterialEditText passwordAgainEditText;

    private String password;
    private String passwordAgain;

    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;
    private ParseUser currentUser;


    public CustomPasswordPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPersistent(false);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.preference_password_dialog, null);
        Log.d(RoadPalApplication.TAG, "onPrepareDialogBuilder ");
        passwordEditText = (MaterialEditText) rootView.findViewById(R.id.password_edit_text);
        passwordAgainEditText = (MaterialEditText) rootView.findViewById(R.id.password_again_edit_text);

        builder.setView(rootView);
        super.onPrepareDialogBuilder(builder);
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        alertDialog = (AlertDialog) getDialog();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setOnClickListener(this);
        Log.d(RoadPalApplication.TAG, "showDialog ");
        passwordAgainEditText.addTextChangedListener(this);
        currentUser = ParseUser.getCurrentUser();
    }

    //not getting called
        /*  @Override
        protected void onBindDialogView(View view) {
            super.onBindDialogView(view);
            Log.d(RoadPalApplication.TAG, "onBindDialogView ");

    }*/
//TODO implement mvp pattern
    @Override
    public void afterTextChanged(Editable s) {

        password = passwordEditText.getText().toString().trim();
        passwordAgain = passwordAgainEditText.getText().toString().trim();
        //  if (password.length() == passwordAgain.length()) {
        if (password.equals(passwordAgain)) {
            //TODO either show error message below password again(text view) or implement it inside edittext controls(passwordEditText and passwordAgainEditText)
            Log.d(RoadPalApplication.TAG, "Password preference dialog passwords are equal");
        } else {
            passwordAgainEditText.setError("Passwords do not match!");
            Log.d(RoadPalApplication.TAG, "Password preference dialog passwords are NOT equal");
        }
        //  }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    //TODO implement mvp pattern and progress dialog upon updating password to parse
    @Override
    public void onClick(View v) {
        Log.d(RoadPalApplication.TAG, "dialog ok onclick :");
        showProgress();
        if (passwordAgainEditText.getError() == null) {
            currentUser.setPassword(passwordAgain);
            currentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d(RoadPalApplication.TAG, "Succesfully  changed password! ");
                        hideProgress();
                        onDialogClosed(true);
                        alertDialog.dismiss();
                        ParseUser.logOut();
                        openLoginActivity();

                    } else {
                        Log.d(RoadPalApplication.TAG, "Error on changing password occured: " + e.getLocalizedMessage());
                        //TODO if for example there is no net connection user will not be able to dismiss change password dialog
                        hideProgress();
                    }
                }
            });
        }
    }

    public void showProgress() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getContext().getString(R.string.progress_password_change));
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void openLoginActivity() {
        Context context = getContext();
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        ((Activity) context).finish();

    }

}
