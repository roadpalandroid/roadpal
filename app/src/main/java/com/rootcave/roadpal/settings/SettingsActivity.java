package com.rootcave.roadpal.settings;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 4.12.2015..
 */
public class SettingsActivity extends BaseActivity{

    //public CustomPasswordPreference mPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        super.onCreateToolbar();
       // addPreferencesFromResource(R.xml.preferences);

        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame,
                        new MainSettingsFragment()).commit();
    }
    public static class MainSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, ISettingsView {
        private ISettingsPresenter mPresenter;
        private ProgressDialog progressDialog;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            mPresenter = new  SettingsPresenterImpl(this);
            Log.d(RoadPalApplication.TAG, "inside on create!");

        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
         mPresenter.processSharedPreferences(sharedPreferences, key);


        }

        @Override
        public void onSaveToParseSucess() {

        }

        @Override
        public void onSaveToParseFailure(ParseException e) {

        }

        @Override
        public void showProgress() {

        }

        @Override
        public void hideProgress() {

        }
    }
}
