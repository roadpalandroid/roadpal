package com.rootcave.roadpal.settings;

import android.content.SharedPreferences;
import android.util.Log;

import com.rootcave.roadpal.common.RoadPalApplication;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Class for saving(updating) settings preferences to parse
 */
public class SettingsInteractorImpl implements ISettingsInteractor {

    private OnProcessPreferencesCompleteListener mlistener;
    private ParseUser currentUser;
    private Boolean haveVehicle = false;

    public SettingsInteractorImpl(OnProcessPreferencesCompleteListener listener) {
        mlistener = listener;
    }


    @Override
    public void processSharedPrefs(SharedPreferences preferences, String key) {

        Log.d(RoadPalApplication.TAG, "preference changed: " + key);

        switch (key) {
            case RoadPalApplication.PREFERENCE_PUSH_NOTIFICATION:

                Log.d(RoadPalApplication.TAG, "push preference changed: " + preferences.getBoolean(key, true));
                Boolean pushEnabled = preferences.getBoolean(key, true);
                saveKeyValuePairToParse(RoadPalApplication.KEY_PUSH, pushEnabled);
                //TODO  implement push
                break;


            case RoadPalApplication.PREFERENCE_THEME:
                //TODO  implement theme change
                break;

            case RoadPalApplication.PREFERENCE_PASSWORD:
                //Change Password gets called in CustomPasswordPreference class, this case is here just for reference
                break;

            case RoadPalApplication.PREFERENCE_MOBILE_NUMBER:
                String mobileNumber = preferences.getString(key, "empty");
                saveKeyValuePairToParse(RoadPalApplication.KEY_PHONE, mobileNumber);
                break;

            case RoadPalApplication.PREFERENCE_EMAIL:
                String email = preferences.getString(key, "empty");
                saveKeyValuePairToParse(RoadPalApplication.KEY_EMAIL, email);
                break;

            case RoadPalApplication.PREFERENCE_HAVE_VEHICLE:
                haveVehicle = preferences.getBoolean(key, false);
                Log.d(RoadPalApplication.TAG, " haveVehicle:  " + haveVehicle);
                // if have vehicle is unchecked delete vehicle model and plates from parse and from settings
                //TODO  user enters vehicle model and plate , then unchecks (model and plate are deleted), then checks again and values are still there
                if (!haveVehicle) {
                    String emptyString = "";
                    saveKeyValuePairToParse(RoadPalApplication.KEY_VEHICLE_MODEL, emptyString);
                    saveKeyValuePairToParse(RoadPalApplication.KEY_VEHICLE_PLATES, emptyString);

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(RoadPalApplication.PREFERENCE_VEHICLE_MODEL, emptyString);
                    editor.putString(RoadPalApplication.PREFERENCE_VEHICLE_PLATES, emptyString);
                    editor.commit();
                    Log.d(RoadPalApplication.TAG, " model and plates deleted! ");
                }
                break;

            case RoadPalApplication.PREFERENCE_VEHICLE_MODEL:
                if (haveVehicle) {
                    String vehicleModel = preferences.getString(key, "empty");
                    saveKeyValuePairToParse(RoadPalApplication.KEY_VEHICLE_MODEL, vehicleModel);
                    Log.d(RoadPalApplication.TAG, " case PREFERENCE_VEHICLE_MODEL:  ");
                }
                break;


            case RoadPalApplication.PREFERENCE_VEHICLE_PLATES:
                if (haveVehicle) {
                    String vehiclePlates = preferences.getString(key, "empty");
                    saveKeyValuePairToParse(RoadPalApplication.KEY_VEHICLE_PLATES, vehiclePlates);
                    Log.d(RoadPalApplication.TAG, " case PREFERENCE_VEHICLE_PLATES:  ");
                }
                break;

        }

    }

    private void saveKeyValuePairToParse(final String key, final Object value) {
        currentUser = ParseUser.getCurrentUser();
        currentUser.put(key, value);
        currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d(RoadPalApplication.TAG, "inside saveKeyValuePairToParse: " + key + ": " + value.toString() + " saved succesfully ");
                    //TODO implement sending result back to fragment
                } else {
                    Log.d(RoadPalApplication.TAG, "inside saveKeyValuePairToParse: Error happened: " + e.getLocalizedMessage());

                }
            }
        });


    }


}
