package com.rootcave.roadpal.settings;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 22.12.2015..
 */
public interface ISettingsView {
    void onSaveToParseSucess();
    void onSaveToParseFailure(ParseException e);
     void showProgress();
     void hideProgress();
}
