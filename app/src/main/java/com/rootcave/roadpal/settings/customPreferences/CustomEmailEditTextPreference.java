package com.rootcave.roadpal.settings.customPreferences;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.rootcave.roadpal.common.RoadPalApplication;

public class CustomEmailEditTextPreference extends EditTextPreference
{
    // if true, this preference requires new values to be checked for conformance to e-mail syntax
    private boolean isEmail = true;

    public CustomEmailEditTextPreference(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        // set isEmail either from custom XML-attributes (look up through attrs)
        // or just by key
        // if(getKey().equals(KNOWN_EMAIL_PREF))
        //   isEmail = true;
    }

    /**
     * Checks if newValue conforms to a specific rule/syntax.
     * Returns error code equal to resource ID of corresponding error message if the value is incorrect,
     * or 0 if the validation was successful
     *
     * @param  newValue  a string with new preference value that needs a check-up
     * @return    integer error code equal to error message resource id
     */
    private int isValid(String newValue)
    {
        int result = 0; // no error

        if(isEmail)
        {
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(newValue).matches())
            {
                result =-1; //R.string.invalid_email;
            }
        }
        // ...
        // other check-ups if necessary

        return result;
    }

    @Override
    protected void showDialog(Bundle state)
    {
        super.showDialog(state);

        final AlertDialog dialog = (AlertDialog)getDialog();

        final EditText edit = getEditText();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(RoadPalApplication.TAG,"dialog onclick :"+isValid(edit.getText().toString()));
                int errorCode = isValid(edit.getText().toString());
                Boolean canCloseDialog = (errorCode == 0);

                if(canCloseDialog)
                {
                    dialog.dismiss();
                    onDialogClosed(true);
                }
                else
                {
                    //String errorMessage = getContext().getString(errorCode);
                    String errorMessage = "Not a valid email";
                    Toast t = Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }
        });
    }
}