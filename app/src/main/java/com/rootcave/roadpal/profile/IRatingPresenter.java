package com.rootcave.roadpal.profile;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 5.4.2016..
 */
public interface IRatingPresenter {


    void fetchRating(ArrayList<RideRatingModel> listOfRatings);
}
