package com.rootcave.roadpal.profile;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.PagerAdapter;
import com.rootcave.roadpal.common.ZoomOutPageTransformer;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 4.12.2015..
 */
public class ProfileActivity extends BaseActivity implements TabLayout.OnTabSelectedListener{

    @Bind(R.id.profile_tab_layout) TabLayout tabLayout;
    @Bind(R.id.profile_pager) ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        super.onCreateToolbar();
        ButterKnife.bind(this);
        tabLayout = (TabLayout) findViewById(R.id.profile_tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Info."));
        tabLayout.addTab(tabLayout.newTab().setText("Rating"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        ArrayList<Fragment> mFragmentlist = new ArrayList<>();


        mFragmentlist.add(InfoFragment.newInstance());
        mFragmentlist.add(RatingFragment.newInstance());


        viewPager = (ViewPager) findViewById(R.id.profile_pager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(),this,mFragmentlist);
        viewPager.setAdapter(adapter);

        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(this);



    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
    viewPager.setCurrentItem(tab.getPosition());
}

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
