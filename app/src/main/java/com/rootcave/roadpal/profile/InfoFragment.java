package com.rootcave.roadpal.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rootcave.roadpal.R;

import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 24.3.2016..
 */
public class InfoFragment extends Fragment{

    public static InfoFragment newInstance()
    {
        InfoFragment fragment= new InfoFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);


        return view;
    }
}
