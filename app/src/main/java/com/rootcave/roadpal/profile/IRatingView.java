package com.rootcave.roadpal.profile;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 5.4.2016..
 */
public interface IRatingView {

    void onProcessedDataError(ParseException e);
    void onProcessedDataSucess();
    void onProcessedDataNoResults();
    void showProgress();
    void hideProgress();


}
