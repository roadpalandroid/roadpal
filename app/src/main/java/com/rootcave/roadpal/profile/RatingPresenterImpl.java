package com.rootcave.roadpal.profile;

import com.parse.ParseException;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 5.4.2016..
 */
public class RatingPresenterImpl implements IRatingPresenter, IonRatingFinishedListener {
    private IratingInteractor mRatingInteractor;
    private IRatingView mRatingView;


    public RatingPresenterImpl(IRatingView ratingView) {


        this.mRatingInteractor = new RatingInteractorImpl(this);
        this.mRatingView = ratingView;


    }

    @Override
    public void fetchRating(ArrayList<RideRatingModel> listOfRatings) {
        mRatingInteractor.fetchRating(listOfRatings);
        mRatingView.showProgress();
    }


    @Override
    public void onFetchRatingSucess() {
        //TODO perhaps add progress bar
        mRatingView.onProcessedDataSucess();
    }

    @Override
    public void onFetchRatingNoResults() {
        mRatingView.onProcessedDataNoResults();
    }

    @Override
    public void onFetchRatingFailure(ParseException e) {
        mRatingView.onProcessedDataError(e);
    }
}
