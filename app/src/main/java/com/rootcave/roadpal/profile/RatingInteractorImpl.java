package com.rootcave.roadpal.profile;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 5.4.2016..
 */
public class RatingInteractorImpl implements IratingInteractor {
    private IonRatingFinishedListener mListener;
    private ParseUserModel currentUserModel = new ParseUserModel(ParseUser.getCurrentUser());

    public RatingInteractorImpl(IonRatingFinishedListener listener) {


        this.mListener = listener;


    }

    @Override
    public void fetchRating(final ArrayList<RideRatingModel> listOfRatings) {

        //TODO just in case implement clearing of list before populating it in another interactors(MyRidesInteractor etc...)
        listOfRatings.clear();

        ParseQuery<RideRatingModel> query = ParseQuery.getQuery(RideRatingModel.class);
        query.whereEqualTo("user_id", currentUserModel.get_object_id());
       // query.orderByDescending("user_rating");
        query.findInBackground(new FindCallback<RideRatingModel>() {

            @Override
            public void done(List<RideRatingModel> objects, ParseException e) {
                Log.d(RoadPalApplication.TAG, "userObjectId: " + currentUserModel.get_object_id());
                Log.d(RoadPalApplication.TAG, "Size of ride rating model list: " + objects.size());
                if (e == null) {
                    if (objects.size() > 0) {
                        listOfRatings.addAll(objects);
                        mListener.onFetchRatingSucess();

                    } else {
                        mListener.onFetchRatingNoResults();
                    }

                } else {
                    Log.d(RoadPalApplication.TAG, "Find in background error : " + e.getLocalizedMessage());
                    mListener.onFetchRatingFailure(e);
                }


            }
        });


    }
}
