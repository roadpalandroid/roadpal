package com.rootcave.roadpal.profile;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.parse.ParseException;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.RoadPalApplication;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 24.3.2016..
 */
public class RatingFragment extends Fragment implements IRatingView, RatingBar.OnRatingBarChangeListener, Button.OnClickListener {


    @Bind(R.id.textViewNumberRating)
    TextView numberRatingTextView;
    @Bind(R.id.ratingBar)
    RatingBar ratingBar;
    //   @Bind(R.id.btnSubmit)
    //  Button btnSubmit;
    @Bind(R.id.horizontal_bar_chart)
    HorizontalBarChart horizontalBarChart;

    private IRatingPresenter mRatingPresenter;
    private ArrayList<RideRatingModel> listOfRatings = new ArrayList<RideRatingModel>();
    private BarData data;


    public static RatingFragment newInstance() {
        RatingFragment fragment = new RatingFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        mRatingPresenter = new RatingPresenterImpl(this);
        mRatingPresenter.fetchRating(listOfRatings);
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rating, container, false);
        ButterKnife.bind(this, view);

        ratingBar.setOnRatingBarChangeListener(this);
        // btnSubmit.setOnClickListener(this);
        buildHorizontalBarChart();
        return view;
    }

    private void buildHorizontalBarChart() {
//TODO Refactor by adding more helper methods instead of big one
        //  ArrayList<BarEntry> listOfBarEntries = populateListOfBarRatings();
        //listOfBarEntries.get(0).getVal()


        ArrayList<BarEntry> listOfBarEntries = new ArrayList<>();

      /*  listOfBarEntries.add(new BarEntry(4f, 0));
        listOfBarEntries.add(new BarEntry(8f, 1));
        listOfBarEntries.add(new BarEntry(6f, 2));
        listOfBarEntries.add(new BarEntry(12f, 3));
        listOfBarEntries.add(new BarEntry(18f, 4));*/


        listOfBarEntries.clear();
        float ratingSum = 0;
        int counterSum = 0;

        float oneStarRating = 0;
        float twoStarRating = 0;
        float threeStarRating = 0;
        float fourStarRating = 0;
        float fiveStarRating = 0;

        for (int i = 0; i < listOfRatings.size(); i++) {
            float rating = listOfRatings.get(i).get_user_rating();

            if (rating < 2) {
                oneStarRating += rating;

            } else if (rating < 3) {
                twoStarRating += rating / 2;

            } else if (rating < 4) {
                threeStarRating += rating / 3;

            } else if (rating < 5) {
                fourStarRating += rating / 4;

            } else {
                fiveStarRating += rating / 5;

            }

            counterSum++;
            ratingSum = ratingSum + listOfRatings.get(i).get_user_rating();
            if (i == listOfRatings.size() - 1) {

                ratingSum = ratingSum / counterSum;
            }
        }


        listOfBarEntries.add(new BarEntry(fiveStarRating, 4));
        listOfBarEntries.add(new BarEntry(fourStarRating, 3));
        listOfBarEntries.add(new BarEntry(threeStarRating, 2));
        listOfBarEntries.add(new BarEntry(twoStarRating, 1));
        listOfBarEntries.add(new BarEntry(oneStarRating, 0));

        Log.d(RoadPalApplication.TAG, "Total sum = " + ratingSum);

        numberRatingTextView.setText(String.format("%.1f",ratingSum));
        ratingBar.setRating(ratingSum);




        if (listOfBarEntries.size() > 0) {
            Log.d(RoadPalApplication.TAG, "value 0 = " + listOfBarEntries.get(0).getVal());
        }

        BarDataSet dataSet = new BarDataSet(listOfBarEntries, "# of Calls");
        dataSet.setColors(new int[]{R.color.azure4, R.color.md_deep_orange_700}, getContext());
        dataSet.setDrawValues(false);
        //TODO add correct colors for chart



        ArrayList<String> labels = new ArrayList<String>();
/*
        labels.add("5 stars");
        labels.add("4 stars");
        labels.add("3 stars");
        labels.add("2 stars");
        labels.add("1 stars");
        */

        labels.add(String.valueOf(Math.round(oneStarRating)));
        labels.add(String.valueOf(Math.round(twoStarRating)));
        labels.add(String.valueOf(Math.round(threeStarRating)));
        labels.add(String.valueOf(Math.round(fourStarRating)));
        labels.add(String.valueOf(Math.round(fiveStarRating)));

        // BarChart chart = new BarChart(getContext());

        data = new BarData(labels, dataSet);
        // chart.setDescription("My rating from other users");

        horizontalBarChart.setData(data);
        horizontalBarChart.setDescription("My rating from other users");
        horizontalBarChart.setBorderColor(Color.RED);
        //horizontalBarChart.re
        horizontalBarChart.setDrawingCacheBackgroundColor(Color.GREEN);

        fineTuneHorizontalBarChart();

    }

    private ArrayList<BarEntry> populateListOfBarRatings() {

        ArrayList<BarEntry> listOfBarEntries = new ArrayList<>();

      /*  listOfBarEntries.add(new BarEntry(4f, 0));
        listOfBarEntries.add(new BarEntry(8f, 1));
        listOfBarEntries.add(new BarEntry(6f, 2));
        listOfBarEntries.add(new BarEntry(12f, 3));
        listOfBarEntries.add(new BarEntry(18f, 4));*/


        listOfBarEntries.clear();
        float ratingSum = 0;
        int counterSum = 0;
        for (int i = 0; i < listOfRatings.size(); i++) {


            //if(listOfRatings)
            float rating = listOfRatings.get(i).get_user_rating();


            listOfBarEntries.add(new BarEntry(rating, i));
            counterSum++;
            ratingSum = ratingSum + listOfRatings.get(i).get_user_rating();
            if (i == listOfRatings.size() - 1) {

                ratingSum = ratingSum / counterSum;
            }
        }


        Log.d(RoadPalApplication.TAG, "Total sum = " + ratingSum);
        ratingBar.setRating(ratingSum);

        return listOfBarEntries;
    }


    private void fineTuneHorizontalBarChart() {
        horizontalBarChart.getAxisLeft().setDrawLabels(false);
        horizontalBarChart.getAxisLeft().setDrawZeroLine(false);
        horizontalBarChart.getAxisLeft().setDrawAxisLine(false);
        horizontalBarChart.getAxisLeft().setDrawGridLines(false);

        horizontalBarChart.getAxisRight().setDrawLabels(false);
        horizontalBarChart.getAxisRight().setDrawZeroLine(false);
        horizontalBarChart.getAxisRight().setDrawAxisLine(false);
        horizontalBarChart.getAxisRight().setDrawGridLines(false);

        horizontalBarChart.getXAxis().setDrawAxisLine(false);
        horizontalBarChart.getXAxis().setDrawGridLines(false);

    }

    //RatingBar.OnRatingBarChangeListener
    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        //textViewRatingValue.setText(String.valueOf(rating));
    }

    //Button.OnClickListener
    @Override
    public void onClick(View v) {

        Log.d(RoadPalApplication.TAG, "Rating: " + String.valueOf(ratingBar.getRating()));
    }


    @Override
    public void onProcessedDataSucess() {


        horizontalBarChart.clear();
        // horizontalBarChart.setData(data);
        buildHorizontalBarChart();

    }

    @Override
    public void onProcessedDataNoResults() {

    }

    @Override
    public void onProcessedDataError(ParseException e) {

//TODO implement proper error handling
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
