package com.rootcave.roadpal.profile;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 5.4.2016..
 */
public interface IonRatingFinishedListener {

    void onFetchRatingSucess();
    void onFetchRatingNoResults();
    void onFetchRatingFailure(ParseException e);

}
