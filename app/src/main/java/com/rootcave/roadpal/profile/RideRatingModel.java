package com.rootcave.roadpal.profile;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by Lenovo T420 on 14.5.2016..
 */
@ParseClassName("RideRating")
public class RideRatingModel extends ParseObject {


    public RideRatingModel() {
    }

    public String getUser_id(){return getString("user_id");}
    public float get_user_rating(){return getNumber("user_rating").floatValue();}





    public void setUser_id(String user_id) {put("user_id",user_id);}
    public void set_user_rating(float user_rating){put("user_rating",user_rating);}


}
