package com.rootcave.roadpal.ride.rideDetail;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.LifecycleLoggingActivity;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.myRides.pendingUsers.PendingRequestsActivity;

import de.greenrobot.event.EventBus;


/**
 * Created by Lenovo T420 on 6.1.2016..
 */
public class RideDetailActivity extends LifecycleLoggingActivity implements RideDetailsFragment.Callbacks {
    private RideDetailsFragment mFragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_detail);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final FragmentManager fm = getSupportFragmentManager();
        mFragment = (RideDetailsFragment) fm.findFragmentByTag(RideDetailsFragment.TAG);

        if (mFragment == null) {
            mFragment = new RideDetailsFragment();
            RideModel model = EventBus.getDefault().getStickyEvent(RideModel.class);
            mFragment.setRideModel(model);
            Log.d(RoadPalApplication.TAG, "new RideDetailsFragment created");


            fm.beginTransaction().add(R.id.frag_item_detail_container, mFragment, RideDetailsFragment.TAG).commit();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            // in case of small screens this activity is called from FoundRidesActivity and from MyRidesActivity
            finish();
          /*  NavUtils.navigateUpTo(this,
                    new Intent(this, MyRidesActivity.class));*/
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(RideModel rideModel) {
        launchPendingRequestsActivity(rideModel);
        Log.d(RoadPalApplication.TAG, "Pending request activity launched from ride detail activity! ");

    }

    private void launchPendingRequestsActivity(RideModel model) {
        EventBus.getDefault().postSticky(model);
        Intent userIntent= new Intent(this, PendingRequestsActivity.class);
        startActivity(userIntent);
        Log.d(RoadPalApplication.TAG, "Pending request activity launched from ride detail activity! ");

    }
}
