package com.rootcave.roadpal.ride;

import android.util.Log;

import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 10.12.2015..
 */
public class PlaceAPI {

    private static final String TAG = PlaceAPI.class.getSimpleName();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

  public ArrayList<LocationPointModel> autocomplete(String input) {
        ArrayList<LocationPointModel> locationPointModelList =null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + RoadPalApplication.GOOGLE_API_KEY);
            //   sb.append("&types=(cities)"); returns only cities results
           //    sb.append("&language=hr");    // for specific language, en for english:    https://developers.google.com/maps/faq#languagesupport
           //TODO caution when changing language: from and to fields in create ride are implemented like primary keys for finding LocationPointModel later on.Take a look at CreateRideInteractorImpl
            sb.append("&language=en");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            Log.d(RoadPalApplication.TAG, "query url: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return locationPointModelList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return locationPointModelList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            // Log.d(RoadPalApplication.TAG,"Json results:  "+jsonResults.toString());   //complete json result
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            locationPointModelList = new ArrayList<>();
            for (int i = 0; i < predsJsonArray.length(); i++) {

                // each json result has list of different types. Algorithm below checks if there are results with types "natural_feature" or "country".
               //  if such results are found they are excluded from list which is sent back to PlacesAutoCompleteAdapter
                JSONArray typesJsonArray = predsJsonArray.getJSONObject(i).getJSONArray("types");
                int counter = 0;
                for (int j = 0; j < typesJsonArray.length(); j++) {
                    String type = typesJsonArray.getString(j);
                    if (!type.equals("natural_feature") && !type.equals("country")) {
                        counter++;
                    }
                }
                if (counter == typesJsonArray.length()) {
                    String description;
                    String place_id;
                    String types;
                    description=predsJsonArray.getJSONObject(i).getString("description");
                    place_id=predsJsonArray.getJSONObject(i).getString("place_id");
                    types = predsJsonArray.getJSONObject(i).getString("types");

                    LocationPointModel locationPoint= new LocationPointModel();
                    locationPoint.setPlaceId(place_id);
                    locationPoint.setAdress_description(description);
                    locationPoint.setTypes(types);
                    locationPointModelList.add(locationPoint);

                    Log.d(RoadPalApplication.TAG, "place_id : " + predsJsonArray.getJSONObject(i).getString("place_id"));
                    Log.d(RoadPalApplication.TAG, "description : " + predsJsonArray.getJSONObject(i).getString("description"));
                    Log.d(RoadPalApplication.TAG, "types : " + types);

                }

            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
      return locationPointModelList;
    }
}