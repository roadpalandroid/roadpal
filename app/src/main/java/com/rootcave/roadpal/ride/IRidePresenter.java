package com.rootcave.roadpal.ride;

import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 26.2.2016..
 */
public interface IRidePresenter {


    ArrayList<LocationPointModel> autocomplete(String constraint);

}
