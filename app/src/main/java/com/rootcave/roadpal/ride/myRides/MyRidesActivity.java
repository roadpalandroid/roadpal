package com.rootcave.roadpal.ride.myRides;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.PagerAdapter;
import com.rootcave.roadpal.common.RoadPalApplication;

import com.rootcave.roadpal.common.ZoomOutPageTransformer;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.myRides.pendingUsers.PendingRequestsActivity;
import com.rootcave.roadpal.ride.rideDetail.RideDetailActivity;
import com.rootcave.roadpal.ride.rideDetail.RideDetailsFragment;
import com.kogitune.activity_transition.fragment.FragmentTransitionLauncher;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by Lenovo T420 on 4.12.2015..
 */
public class MyRidesActivity extends BaseActivity implements HostedRidesListFragment.Callbacks,AcceptedRidesListFragment.Callbacks,RideDetailsFragment.Callbacks,PendingRidesListFragment.Callbacks,TabLayout.OnTabSelectedListener {

    @Bind(R.id.my_rides_pager) ViewPager viewPager;
    @Bind(R.id.my_rides_tab_layout) TabLayout tabLayout;

    private HostedRidesListFragment mFragment;
    private boolean mTwoPane;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rides);
        super.onCreateToolbar();
        ButterKnife.bind(this);
        tabLayout.addTab(tabLayout.newTab().setText("Hosted rides"));
        tabLayout.addTab(tabLayout.newTab().setText("Accepted Rides"));
        tabLayout.addTab(tabLayout.newTab().setText("Pending rides"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        ArrayList<Fragment>   mFragmentlist = new ArrayList<>();
        mFragmentlist.add(HostedRidesListFragment.newInstance());
        mFragmentlist.add(AcceptedRidesListFragment.newInstance());
        mFragmentlist.add(PendingRidesListFragment.newInstance());

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(),this,mFragmentlist);
        viewPager.setAdapter(adapter);

        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(this);



/*
        FragmentManager fm = getSupportFragmentManager();
        mFragment = (HostedRidesListFragment) fm.findFragmentByTag(HostedRidesListFragment.TAG);
        if (mFragment == null) {
            mFragment = new HostedRidesListFragment();
            Log.d(RoadPalApplication.TAG, "new HostedRidesListFragment created");
            fm.beginTransaction().add(R.id.frag_list_container, mFragment, HostedRidesListFragment.TAG).commit();
        }

        if (findViewById(R.id.frag_item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
            Log.d(RoadPalApplication.TAG, "Mtwopane == true");
        }
*/

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        Log.d(RoadPalApplication.TAG, "Tab " + tab.getPosition() + " selected!");
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }



    //from HostedRidesListFragment.Callbacks  and AcceptedRidesListFragment.Callbacks and PendingRidesListFragment.Callbacks
    @Override
    public void onItemSelected(RideModel model, View view) {
        Log.d(RoadPalApplication.TAG, "Item clicked: " + model.getRideOwner().get_username());
              /*  if (mTwoPane) {
                    launchRideDetailsFragment(model, view);
                } else {*/
                    launchRideDetailActivity(model, view);
             //   }

    }
    // from RideDetailsFragment.Callbacks
    @Override
    public void onItemSelected(RideModel rideModel) {
        //from RideDetailsFragment in two pane mode
        launchPendingRequestsActivity(rideModel);
    }



    private void launchRideDetailActivity(RideModel model, View view) {

        // In single-pane mode, simply start the detail activity
        //sends RideModel to RideDetailActivity via EventBus
        //implemented this way because RideModel which inherits from ParseObject cannot be serialized or parcelized
        EventBus.getDefault().postSticky(model);
        Intent detailIntent = new Intent(this, RideDetailActivity.class);
        Log.d(RoadPalApplication.TAG, "model.get_ObjectId(): " + model.get_ObjectId());
        startActivity(detailIntent);

    }


    private void launchPendingRequestsActivity(RideModel model) {
        EventBus.getDefault().postSticky(model);
        Intent userIntent= new Intent(this, PendingRequestsActivity.class);
        startActivity(userIntent);
        Log.d(RoadPalApplication.TAG, "Pending request activity launched from my rides activity ! ");

    }


}
