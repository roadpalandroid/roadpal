package com.rootcave.roadpal.ride.myRides.pendingUsers;

import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 27.1.2016..
 */
public interface IPendingUserInteractor {

    void extractUsersData(RideModel rideModel, ArrayList<ParseUserModel> listOfPendingPassengers);
    void acceptPendingPassenger(int position);
    void declinePendingPassenger(int position);
}
