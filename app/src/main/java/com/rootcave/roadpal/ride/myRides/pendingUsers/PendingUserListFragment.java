package com.rootcave.roadpal.ride.myRides.pendingUsers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseException;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 25.1.2016..
 */
public class PendingUserListFragment extends Fragment implements IPendingUsersListView, PendingUserListRecycleViewAdapter.PendingUserListRecycleViewItemClickListener/*, RecyclerItemClickListener.OnItemClickListener*/ {
    public static String TAG = "PendingUserListFragment";

    @Bind(R.id.recycler_view_users)
    RecyclerView usersRecyclerView;

    private PendingUserListRecycleViewAdapter mAdapter;
    private ArrayList<ParseUserModel> listOfPendingPassengers = new ArrayList<>();
    private IPendingUserPresenter mUserPresenter;
    private ProgressDialog progressDialog;
    private RideModel rideModel;

    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(ParseUserModel model, View view);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(ParseUserModel model, View view) {
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        ButterKnife.bind(this, view);

        usersRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        usersRecyclerView.setLayoutManager(mLayoutManager);

        mUserPresenter = new PendingUserPresenterImpl(this);
        mUserPresenter.fetchUsersData(rideModel, listOfPendingPassengers);
        // usersRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    public void setRideModel(RideModel model) {
        this.rideModel = model;


    }

    @Override
    public void onFetchUsersDataSucess() {
        mAdapter = new PendingUserListRecycleViewAdapter(listOfPendingPassengers, this);
        usersRecyclerView.setAdapter(mAdapter);
        Log.d(RoadPalApplication.TAG, "onfetch data sucess!");
    }

    @Override
    public void onFetchUsersDataFailure(ParseException e) {
        Log.d(RoadPalApplication.TAG, "Error happened:" + e.getLocalizedMessage());
    }

    @Override
    public void onProcessPendingPassengerSucess() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onProcessPendingPassengerFailure(ParseException e) {
        Log.d(RoadPalApplication.TAG, "onProcessPendingPassengerFailure: " + e.getLocalizedMessage());
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.progress_search_rides));
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    //custom click listener from PendingUserListRecycleViewAdapter
    @Override
    public void onItemClick(View v, int position) {
        Log.d(RoadPalApplication.TAG, position + "  clicked!");
        Log.d(RoadPalApplication.TAG, "View:" + v.getId());

        // mCallbacks.onItemSelected(model, v);

        switch (v.getId()) {
            case R.id.button_accept_passenger:
                //mCallbacks.onItemSelected(rideModel);

                mUserPresenter.acceptPendingPassenger(position);

                // listOfPendingPassengers.remove(position);
                //   mAdapter.notifyDataSetChanged();
                break;
            case R.id.button_decline_passenger:

                mUserPresenter.declinePendingPassenger(position);
                break;
            case R.id.clickable_relative_layout:
                mCallbacks.onItemSelected(listOfPendingPassengers.get(position), v);
                break;
        }


    }
}
