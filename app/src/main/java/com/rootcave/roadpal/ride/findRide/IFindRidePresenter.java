package com.rootcave.roadpal.ride.findRide;

import com.rootcave.roadpal.ride.IRidePresenter;
import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 14.12.2015..
 */
public interface IFindRidePresenter extends IRidePresenter {

    void processInputData(RideModel findModelQueryData,ArrayList<RideModel> listOfRides);

}
