package com.rootcave.roadpal.ride.createRide;

/**
 * Created by Lenovo T420 on 9.12.2015..
 */
public interface IonCreateRideFinishedListener {
    void onCreateRideValidationSucess();
    void onCreateRideValidationFailure();
    void onSaveModelToParseSucess();
    void onSaveModelToParseFailure();
}
