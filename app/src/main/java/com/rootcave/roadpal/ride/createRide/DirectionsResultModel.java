package com.rootcave.roadpal.ride.createRide;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by Lenovo T420 on 12.3.2016..
 */
public class DirectionsResultModel {
    @Key("routes")
    private List<Route> routes;
    public List<Route> getRoutes() {
        return routes;
    }

    public static class Route {
        @Key("legs")
        private List<Leg> legs;
        public List<Leg> getLegs() {return legs;}
    }

    public static class Leg {
        @Key("distance")
        private Distance distance;

        @Key("duration")
        private Duration duration;

        public Distance getDistance() {
            return distance;
        }
        public Duration getDuration() {
            return duration;
        }

    }

    public static class Distance {
        @Key
        private String text;
        @Key
        private int value;

        public int getValue() {return value;}
        public String getText() {return text;}
    }

    public static class Duration {
        @Key
        private String text;
        @Key
        private int value;

        public String getText() {return text;}
        public int getValue() {return value;}

    }
}
