package com.rootcave.roadpal.ride.createRide;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.ride.PlacesAutoCompleteAdapter;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.customViews.FancyCoverFlow;
import com.rootcave.roadpal.ride.customViews.ViewGroupExampleAdapter;
import com.parse.ParseException;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Activity which creates new ride
 */
public class CreateRideActivity extends BaseActivity implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, View.OnClickListener, ICreateRideView, AdapterView.OnItemClickListener {

    @Bind(R.id.date_edit_text)
    MaterialEditText dateEditText;
    @Bind(R.id.time_edit_text)
    MaterialEditText timeEditText;
    @Bind(R.id.use_highways_check_box)
    CheckBox useHighwaysCheckBox;
    @Bind(R.id.fancyCoverFlow)
    FancyCoverFlow fancyCoverFlow;
    @Bind(R.id.vehicle_type_edit_text)
    MaterialEditText vehicleTypeEditText;
    @Bind(R.id.vehicle_plate_number_edit_text)
    MaterialEditText vehiclePlateNumberEditText;
    @Bind(R.id.available_seats_edit_text)
    MaterialEditText availableSeatsEditText;
    @Bind(R.id.button_done)
    Button buttonDone;
    @Bind(R.id.autocomplete_text_view_from)
    AutoCompleteTextView autoCompleteTextViewFrom;
    @Bind(R.id.autocomplete_text_view_to)
    AutoCompleteTextView autoCompleteTextViewTo;

    private ICreateRidePresenter mCreateRidePresenter;
    private ProgressDialog progressDialog;
    private String from;
    private String to;
    private String genderAllowed;
    private String vehicleType;
    private String plateNumber;
    private int availableSeats;
    private Date dateSelected = null;
    private boolean   useHighways;
    // private PlacesAutoCompleteAdapter adapterFrom;
   // private PlacesAutoCompleteAdapter adapterTo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ride);
        super.onCreateToolbar();
        ButterKnife.bind(this);
        mCreateRidePresenter = new CreateRidePresenterImpl(this);
        fancyCoverFlow.setAdapter(new ViewGroupExampleAdapter());
        ParseUserModel currentUser = RoadPalApplication.getCurrentUser();



/*      // for custom values
        fancyCoverFlow = new FancyCoverFlow(context);
        fancyCoverFlow.setMaxRotation(45);
        fancyCoverFlow.setUnselectedAlpha(0.3f);
        fancyCoverFlow.setUnselectedSaturation(0.0f);
        fancyCoverFlow.setUnselectedScale(0.4f);
*/
        //adapterFrom = new PlacesAutoCompleteAdapter(this, mCreateRidePresenter);
        //adapterTo = new PlacesAutoCompleteAdapter(this, mCreateRidePresenter);


        autoCompleteTextViewFrom.setAdapter(new PlacesAutoCompleteAdapter(this, mCreateRidePresenter));
        autoCompleteTextViewTo.setAdapter(new PlacesAutoCompleteAdapter(this, mCreateRidePresenter));
        // autoCompleteTextViewTo.setAdapter(adapterFrom);

        autoCompleteTextViewFrom.setOnItemClickListener(this);
        autoCompleteTextViewTo.setOnItemClickListener(this);

        //TODO  fetch this two fields from interactor?
        vehicleTypeEditText.setText(currentUser.get_vehicle_model());
        vehiclePlateNumberEditText.setText(currentUser.get_vehicle_plates());


        dateEditText.setOnClickListener(this);
        timeEditText.setOnClickListener(this);
        buttonDone.setOnClickListener(this);
    }

    //AdapterView.OnItemClickListener
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(RoadPalApplication.TAG, "position: " + position);
        PlacesAutoCompleteAdapter adapter = (PlacesAutoCompleteAdapter) parent.getAdapter();
        String description= adapter.getLocationPointModelList().get(position).getAdress_description();
        String item_id = adapter.getLocationPointModelList().get(position).getPlace_id();
        Log.d(RoadPalApplication.TAG, "description: " + description + " item_id: " + item_id);

        mCreateRidePresenter.fetchGeoPoints(adapter.getLocationPointModelList().get(position));



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.date_edit_text:
                createDatePickerDialog();
                break;
            case R.id.time_edit_text:
                createTimePickerDialog();
                break;
            case R.id.button_done:

                from = autoCompleteTextViewFrom.getText().toString().trim();
                to = autoCompleteTextViewTo.getText().toString().trim();
                genderAllowed = fancyCoverFlow.getSelectedItem().toString();

                vehicleType = vehicleTypeEditText.getText().toString().trim();
                plateNumber = vehiclePlateNumberEditText.getText().toString().trim();
                availableSeats = Integer.valueOf(availableSeatsEditText.getText().toString().trim());
                useHighways = useHighwaysCheckBox.isChecked();


                RideModel rideModel = new RideModel();

                rideModel.setFrom(from);
                rideModel.setTo(to);
                rideModel.setUseHighways(useHighways);
                rideModel.setGenderAllowed(genderAllowed);
                rideModel.setVehicleType(vehicleType);
                rideModel.setPlateNumber(plateNumber);
                rideModel.setAvailableSeats(availableSeats);
                rideModel.setDate(dateSelected);

                mCreateRidePresenter.processInputData(rideModel);

                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Log.d(RoadPalApplication.TAG, "year: " + year + " Month: " + monthOfYear + " day: " + dayOfMonth);
        // dateSelected= new Date(year,monthOfYear,dayOfMonth);
        dateSelected = new Date();
        dateSelected.setYear(year - 1900);
        dateSelected.setMonth(monthOfYear);
        dateSelected.setDate(dayOfMonth);
        String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        dateEditText.setText(date);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        // dateSelected.
        Log.d(RoadPalApplication.TAG, "hour: " + hourOfDay + " minute: " + minute + " second: " + second);
        TimeZone tz = TimeZone.getDefault();
        int gmtTimezone = (int) TimeUnit.HOURS.convert(tz.getRawOffset(), TimeUnit.MILLISECONDS);
        if (dateSelected == null) {
            dateSelected = new Date();
        }
        dateSelected.setHours(hourOfDay + gmtTimezone);
        dateSelected.setMinutes(minute);
        dateSelected.setSeconds(second);

        Log.d(RoadPalApplication.TAG, "finalDate:" + dateSelected);
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = hourString + "h" + minuteString + "m" + secondString + "s";
        timeEditText.setText(time);
    }

    @Override
    public void onProcessedDataError(ParseException e) {

    }

    @Override
    public void onProcessedDataSucess() {

    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(CreateRideActivity.this);
        progressDialog.setMessage(getString(R.string.progress_login));

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void navigateToRideDetailActivity() {

    }

    private void createDatePickerDialog() {
        Log.d(RoadPalApplication.TAG, "date picker edit text listener! ");
        Calendar date = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                CreateRideActivity.this,
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH)
        );
        dpd.showYearPickerFirst(false);
        dpd.setThemeDark(true);
        dpd.setTitle("Please select date");
        dpd.setOkText("Ok text");
        dpd.setCancelText("Cancel text");
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private void createTimePickerDialog() {
        Calendar time = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                CreateRideActivity.this,
                time.get(Calendar.HOUR_OF_DAY),
                time.get(Calendar.MINUTE), true
        );
        tpd.setThemeDark(true);
        tpd.setTitle("Please select time");


        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });

        tpd.show(getFragmentManager(), "Timepickerdialog");
    }


}
