package com.rootcave.roadpal.ride.createRide;

import com.rootcave.roadpal.ride.IRidePresenter;
import com.rootcave.roadpal.ride.RideModel;

/**
 * Created by Lenovo T420 on 9.12.2015..
 */
public interface ICreateRidePresenter extends IRidePresenter {

    void processInputData(RideModel rideModel);
    void fetchGeoPoints(LocationPointModel locationPoint);


}
