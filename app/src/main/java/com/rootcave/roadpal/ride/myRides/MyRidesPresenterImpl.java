package com.rootcave.roadpal.ride.myRides;

import com.rootcave.roadpal.ride.IFindRideView;
import com.rootcave.roadpal.ride.IonFindRidesFinishedListener;
import com.rootcave.roadpal.ride.RideModel;
import com.parse.ParseException;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 17.12.2015..
 */
public class MyRidesPresenterImpl implements IMyRidesPresenter, IonFindRidesFinishedListener {

    private IFindRideView mFindRideView;
    private IMyRidesInteractor mMyRidesInteractor;

    public MyRidesPresenterImpl(IFindRideView findRideView) {
        this.mFindRideView = findRideView;
        this.mMyRidesInteractor = new MyRidesInteractorImpl(this);
    }

    @Override
    public void fetchMyHostedRides(ArrayList<RideModel> listOfRides) {
        mMyRidesInteractor.fetchMyHostedRides(listOfRides);
        mFindRideView.showProgress();
    }

    @Override
    public void fetchAcceptedRides(ArrayList<RideModel> listOfRides) {
        mMyRidesInteractor.fetchAcceptedRides(listOfRides);
        mFindRideView.showProgress();
    }

    @Override
    public void fetchPendingdRides(ArrayList<RideModel> listOfRides) {
        mMyRidesInteractor.fetchPendingRides(listOfRides);
        mFindRideView.showProgress();
    }

    @Override
    public void onFindRidesSucess() {
        mFindRideView.onFindRideSucess();
        mFindRideView.hideProgress();
    }

    @Override
    public void onFindRidesNoResults() {
        mFindRideView.hideProgress();
    }

    @Override
    public void onFindRidesFailure(ParseException e) {
        mFindRideView.hideProgress();
    }
}
