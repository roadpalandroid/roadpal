package com.rootcave.roadpal.ride.myRides.pendingUsers;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.LifecycleLoggingActivity;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;

import de.greenrobot.event.EventBus;

/**
 * Created by Lenovo T420 on 26.1.2016..
 */
public class PendingUserDetailsActivity extends LifecycleLoggingActivity {

    private PendingUserDetailsFragment mFragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_user_detail);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        FragmentManager fm = getSupportFragmentManager();
        mFragment = (PendingUserDetailsFragment) fm.findFragmentByTag(PendingUserDetailsFragment.TAG);

        if (mFragment == null) {
            mFragment = new PendingUserDetailsFragment();
            ParseUserModel userModel = EventBus.getDefault().getStickyEvent(ParseUserModel.class);
            mFragment.setUserModel(userModel);
            Log.d(RoadPalApplication.TAG, "new PendingUserDetailsFragment created");


            fm.beginTransaction().add(R.id.frag_pending_user_detail_container, mFragment, PendingUserDetailsFragment.TAG).commit();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            // in case of small screens this activity is called from FoundRidesActivity and from MyRidesActivity
            finish();
          /*  NavUtils.navigateUpTo(this,
                    new Intent(this, MyRidesActivity.class));*/
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}