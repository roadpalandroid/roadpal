package com.rootcave.roadpal.ride.findRide;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RecyclerItemClickListener;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.RideModelRecycleViewAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 15.12.2015..
 */
public class FoundRidesListFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener {
    public static String TAG = "FoundRidesListFragment";

    @Bind(R.id.found_rides_list)
    RecyclerView foundRidesRecyclerView;

    private RideModelRecycleViewAdapter mAdapter;
    private ArrayList<RideModel> listOfRides = new ArrayList<RideModel>();

    private Callbacks mCallbacks = sDummyCallbacks;


    public static FoundRidesListFragment newInstance()
    {
        FoundRidesListFragment fragment= new FoundRidesListFragment();
        return fragment;
    }



    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(RideModel model,View view);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(RideModel model,View view) {
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_found_rides, container, false);
        ButterKnife.bind(this, view);

        foundRidesRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        foundRidesRecyclerView.setLayoutManager(mLayoutManager);

        Log.d(RoadPalApplication.TAG,"FoundRidesListFragment size of list:"+listOfRides.size());

        mAdapter = new RideModelRecycleViewAdapter(listOfRides);
        foundRidesRecyclerView.setAdapter(mAdapter);

        foundRidesRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));

        Log.d(RoadPalApplication.TAG, "inside fragmentsonCreateView");
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    public void setRideModelList(ArrayList<RideModel> listOfRides) {
        this.listOfRides = listOfRides;
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.d(RoadPalApplication.TAG, "Item clicked: " + position);
        RideModel model = listOfRides.get(position);
        mCallbacks.onItemSelected(model,view);
       //
       //launchRideDetailsFragment(model);

    }

    public void notifyDataSetChanged() {
        mAdapter.notifyDataSetChanged();

    }
}
