package com.rootcave.roadpal.ride;

import com.rootcave.roadpal.common.ParseUserModel;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Lenovo T420 on 8.12.2015..
 */
@ParseClassName("RideModel")
public class RideModel extends ParseObject {
    public RideModel() {}

    private ParseUserModel rideOwnerUser = null;
    private ArrayList<String> listOfPassengerRequest;
    private ArrayList<String> listOfAcceptedPassengers ;

    public String get_ObjectId() {return getObjectId();}
    public String getFrom() {return getString("from");}
    public String getTo() {return getString("to");}
    public Date getDate() {return getDate("date");}
    public String getGenderAllowed() {return getString("genderAllowed");}
    public String getVehicleType() {return getString("vehicleType");}
    public String getPlateNumber() {return getString("plateNumber");}
    public Integer getAvailableSeats() {return getInt("availableSeats");}
    public ArrayList<String> getListOfPendingPassengersId() {return (ArrayList<String>) get("listOfPendingPassengers");}

    public ArrayList<String> getListOfAcceptedPassengersId() {return (ArrayList<String>) get("listOfAcceptedPassengers");}

    public String getLocationPointerFrom(){return getString("PointFromId");}
    public String getLocationPointerTo(){return getString("PointToId");}


    public ParseUserModel getRideOwner() {
        //TODO memory efficiency(creating new instance each time)?
        return new ParseUserModel(getParseUser("UserObjectPointer"));

    }
    public LocationPointModel getLocationPointFrom(){return (LocationPointModel)getParseObject("PointFromId");}
    public LocationPointModel getLocationPointTo(){return (LocationPointModel)getParseObject("PointToId");}
    public boolean getUseHighways(){return getBoolean("useHighways");}
    public String getdistanceKm(){return getString("distance");}
    public String getTravelTime(){return getString("travel_time");}


    public void setObjectId(String objectId) {put("objectId", objectId);}
    public void setFrom(String from) {
        put("from", from);
    }
    public void setTo(String to) {
        put("to", to);
    }
    public void setDate(Date dateSelected) {
        put("date", dateSelected);
    }
    public void setGenderAllowed(String genderAllowed) {
        put("genderAllowed", genderAllowed);
    }
    public void setVehicleType(String vehicleType) {
        put("vehicleType", vehicleType);
    }
    public void setPlateNumber(String plateNumber) {put("plateNumber", plateNumber);}
    public void setAvailableSeats(Integer availableSeats) {put("availableSeats", availableSeats);}
    public void setListOfPendingPassengersId(ArrayList<String> listOfPendingPassengersId) {
        put("listOfPendingPassengers", listOfPendingPassengersId);}

    public void setPassengerRequest(String userId) {
        listOfPassengerRequest = getListOfPendingPassengersId();
        if (listOfPassengerRequest == null) {
            listOfPassengerRequest = new ArrayList<>();
        }
        listOfPassengerRequest.add(userId);
        //Save list
        put("listOfPendingPassengers", listOfPassengerRequest);
    }

    public void setAcceptedPassenger(String userId) {
        listOfAcceptedPassengers=getListOfAcceptedPassengersId();
        if (listOfAcceptedPassengers == null) {
            listOfAcceptedPassengers = new ArrayList<>();
        }
        listOfAcceptedPassengers.add(userId);
        setAvailableSeats(getAvailableSeats() - 1);
        //Save list
        put("listOfAcceptedPassengers", listOfAcceptedPassengers);
    }
    public void setRideOwner(ParseUserModel rideOwnerUser) {
        this.rideOwnerUser = rideOwnerUser;
        put("UserObjectPointer",rideOwnerUser.getUser());
        put("userObjectId",rideOwnerUser.get_object_id());
    }
    public void setLocationPointFrom(LocationPointModel locationPoint){put("PointFromId",locationPoint);}
    public void setLocationPointTo(LocationPointModel locationPoint){put("PointToId",locationPoint);}
    public void setUseHighways(boolean useHighways){put("useHighways",useHighways);}
    public void setDistanceKm(String distance){put("distance",distance);}
    public void setTravelTime(String travel_time){put("travel_time",travel_time);}

}

