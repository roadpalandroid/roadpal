package com.rootcave.roadpal.ride;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 10.12.2015..
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
    private ArrayList<LocationPointModel> locationPointModelList;
    private Context mContext;
    private IRidePresenter mPresenter;
    private Filter placesFilter;

    public PlacesAutoCompleteAdapter(Context context, IRidePresenter presenter) {
        super(context, R.layout.autocomplete_list_item);
        mContext = context;

        this.mPresenter = presenter;
    }

    public ArrayList<LocationPointModel> getLocationPointModelList() {
        return locationPointModelList;
    }


    @Override
    public int getCount() {
        return locationPointModelList.size();

    }

    @Override
    public String getItem(int position) {
        return locationPointModelList.get(position).getAdress_description();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        PlacesHolder holder = new PlacesHolder();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.autocomplete_list_item, null);

            holder.autoCompleteTextView= (TextView) v.findViewById(R.id.autocompleteText);
            v.setTag(holder);
        }
        else
        {
           holder=(PlacesHolder) v.getTag();
        }

        LocationPointModel locationPoint = locationPointModelList.get(position);
        if(locationPoint!=null)
        {
            holder.autoCompleteTextView.setText(locationPoint.getAdress_description());
        }

        return v;
    }


    @Override
    public Filter getFilter() {
        if (placesFilter == null) {
            placesFilter = new PlacesFilter();
        }
        return placesFilter;
    }

    private static class PlacesHolder {
        public TextView autoCompleteTextView;

    }

    private class PlacesFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null) {
                locationPointModelList = mPresenter.autocomplete(constraint.toString());
                filterResults.values= locationPointModelList;
                filterResults.count= locationPointModelList.size();
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results != null && results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }

}