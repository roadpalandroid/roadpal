package com.rootcave.roadpal.ride.createRide;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.SaveCallback;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.PlaceAPI;
import com.rootcave.roadpal.ride.RideModel;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 9.12.2015..
 */
public class CreateRideInteractorImpl implements ICreateRideInteractor, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();


    private IonCreateRideFinishedListener mlistener;
    private PlaceAPI mPlaceAPI;
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<LocationPointModel> locationPointsList;
    private RideModel mRideModel;

    public CreateRideInteractorImpl(IonCreateRideFinishedListener listener) {
        mlistener = listener;
        mPlaceAPI = new PlaceAPI();
        initGoogleApiClient();
        locationPointsList = new ArrayList<>();



    }


    @Override
    public void validateInputData(RideModel rideModel) {
        //TODO add validation of fields
        saveInputData(rideModel);
    }


    private void saveInputData(RideModel rideModel) {
        mRideModel = rideModel;
        for (LocationPointModel locationPoint : locationPointsList) {
            if (locationPoint.getAdress_description().equals(rideModel.getFrom())) {

                Log.d(RoadPalApplication.TAG, "From Object match found ");
                Log.d(RoadPalApplication.TAG, "from Geo point:  " + locationPoint.getPoint_coordinates().toString());
                Log.d(RoadPalApplication.TAG, "from location point id: " + locationPoint.getObjectId());
                mRideModel.setLocationPointFrom(locationPoint);
            }
            if (locationPoint.getAdress_description().equals(rideModel.getTo())) {

                Log.d(RoadPalApplication.TAG, "To Object match found ");
                Log.d(RoadPalApplication.TAG, "To Geo point: " + locationPoint.getPoint_coordinates().toString());
                mRideModel.setLocationPointTo(locationPoint);
            }
        }
        /* TODO currently there are 3 network requests.
        Firstly 2 LocationPoints are saved, then their objectId is retrieved and set to ride model PointFromId pointer and PointToId pointer
        Try to put 2 location points and ride model to same list and then execute batch save.
        I guess to do that LocationPointModel class would need to have primary key column other than objectId because latter is created after saving to Parse
       */

        mRideModel.setRideOwner(RoadPalApplication.getCurrentUser());

        String adressFrom = rideModel.getLocationPointFrom().getAdress_description();
        String adressTo = rideModel.getLocationPointTo().getAdress_description();
        boolean useHighways = rideModel.getUseHighways();

        new DirectionsFetcher(adressFrom, adressTo, useHighways).execute();
    }


    @Override
    public ArrayList<LocationPointModel> autocomplete(String constraint) {
        return mPlaceAPI.autocomplete(constraint);
    }


    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(RoadPalApplication.getContext())
                .addApi(Places.GEO_DATA_API)
                        //  .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public void fetchGeoPoints(LocationPointModel locationPoint) {
        String place_id = locationPoint.getPlace_id();
        mGoogleApiClient.connect();
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                .getPlaceById(mGoogleApiClient, place_id);
        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);


    }

    private SaveCallback rideModelSaveCallback = new SaveCallback() {
        @Override
        public void done(ParseException e) {

        }
    };
    private SaveCallback locationPointsSaveCallback = new SaveCallback() {
        @Override
        public void done(ParseException e) {

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(RoadPalApplication.TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }

            Place place = places.get(0);
            String place_id = place.getId();
            String adress = place.getAddress().toString();
            LatLng latLng = place.getLatLng();

            Log.d(RoadPalApplication.TAG, "place id: " + place.getId());
            Log.d(RoadPalApplication.TAG, "place coordinates: " + place.getLatLng());
            Log.d(RoadPalApplication.TAG, "place getAddress: " + place.getAddress());
            Log.d(RoadPalApplication.TAG, "place getName: " + place.getName());

            LocationPointModel locationPoint = new LocationPointModel();
            locationPoint.setPlaceId(place_id);
            locationPoint.setAdress_description(adress);
            ParseGeoPoint point = new ParseGeoPoint(latLng.latitude, latLng.longitude);
            locationPoint.setPointCoordinates(point);
            locationPointsList.add(locationPoint);

            places.release();
            mGoogleApiClient.disconnect();
        }
    };

    //GoogleApiClient.ConnectionCallbacks
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(RoadPalApplication.TAG, "Google places api connected!");
    }

    //GoogleApiClient.ConnectionCallbacks
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(RoadPalApplication.TAG, "Google Places API connection suspended.");
    }

    //GoogleApiClient.OnConnectionFailedListener
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(RoadPalApplication.TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());
    }


    private class DirectionsFetcher extends AsyncTask<URL, Integer, Void> implements SaveCallback {


        private String origin;
        private String destination;
        private boolean useHighways = false;

        public DirectionsFetcher(String origin, String destination, boolean useHighways) {
            this.origin = origin;
            this.destination = destination;
            this.useHighways = useHighways;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected Void doInBackground(URL... urls) {
            try {
                HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    }
                });

                GenericUrl url = new GenericUrl("http://maps.googleapis.com/maps/api/directions/json");
                url.put("origin", origin);
                url.put("destination", destination);
                if (!useHighways) {
                    url.put("avoid", "highways");
                }
                //Log.d(RoadPalApplication.TAG, "url relative url: " + url.buildRelativeUrl());

                HttpRequest request = requestFactory.buildGetRequest(url);
                HttpResponse httpResponse = request.execute();

                   String response = httpResponse.parseAsString();
                   Log.d(RoadPalApplication.TAG, "http response parse as string: " + response);
/*
                 DirectionsResultModel directionsResultModel = httpResponse.parseAs(DirectionsResultModel.class);
                Log.d(RoadPalApplication.TAG,"directions result distance text: "+directionsResultModel.getRoutes().get(0).getLegs().get(0).getDistance().getText());

                String distance = directionsResultModel.getRoutes().get(0).getLegs().get(0).getDistance().getText();
                String travel_time = directionsResultModel.getRoutes().get(0).getLegs().get(0).getDuration().getText();
                mRideModel.setDistanceKm(distance);
                mRideModel.setTravelTime(travel_time);
*/
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.d(RoadPalApplication.TAG, "error happened: " + ex.getLocalizedMessage());
            }
            return null;
        }
        protected void onProgressUpdate(Integer... progress) {
        }
        protected void onPostExecute(Void result) {

            mRideModel.saveEventually(this);
        }

        //SaveCallback
        @Override
        public void done(ParseException e) {
            if (e == null) {
                Log.d(RoadPalApplication.TAG, "Ride model saved succesfully!");
                mlistener.onCreateRideValidationSucess();
            } else {
                Log.d(RoadPalApplication.TAG, "error happened: " + e.toString());
            }
    }

    }

}
