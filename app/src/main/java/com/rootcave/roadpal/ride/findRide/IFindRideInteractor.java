package com.rootcave.roadpal.ride.findRide;

import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 14.12.2015..
 */
public interface IFindRideInteractor {
    //TODO  refactor with proper error handling
    void processInputData(RideModel findModelQueryData,ArrayList<RideModel> listOfRides);
    void findRide();

    ArrayList<LocationPointModel> autocomplete(String constraint);
}
