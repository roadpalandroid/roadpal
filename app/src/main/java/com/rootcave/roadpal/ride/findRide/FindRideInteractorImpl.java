package com.rootcave.roadpal.ride.findRide;

import android.util.Log;

import com.parse.ParseUser;
import com.rootcave.roadpal.ride.PlaceAPI;
import com.rootcave.roadpal.common.RoadPalApplication;

import com.rootcave.roadpal.ride.IonFindRidesFinishedListener;
import com.rootcave.roadpal.ride.RideModel;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 14.12.2015..
 */
//TODO  refactor with proper error handling
public class FindRideInteractorImpl implements IFindRideInteractor {

    IonFindRidesFinishedListener mListener;
    private RideModel mFindModelQueryData;
    private ArrayList<RideModel> listOfRides;
    private PlaceAPI mPlaceAPI ;


    public FindRideInteractorImpl(IonFindRidesFinishedListener mListener) {
        this.mListener = mListener;
        mPlaceAPI= new PlaceAPI();

    }

    @Override
    public void processInputData(RideModel findModelQueryData, ArrayList<RideModel> listOfRides) {
        mFindModelQueryData = findModelQueryData;
        this.listOfRides = listOfRides;
        findRide();
    }

    @Override
    public void findRide() {
        listOfRides.clear();
        // ParseUser currentUser = ParseUser.getCurrentUser();
        // Log.d(RoadPalApplication.TAG, "current user : " + currentUser.getUsername());

        ParseQuery<RideModel> query = ParseQuery.getQuery(RideModel.class);
      //  query.whereGreaterThan("date",mFindModelQueryData.getDate());


       // query.whereEqualTo("date", mFindModelQueryData.getDate());
        query.whereNotEqualTo("userObjectId", ParseUser.getCurrentUser().getObjectId());

        query.include("PointFromId");
        query.include("PointToId");
        query.include("UserObjectPointer");

        //  query.whereContains("from", mFindModelQueryData.getFrom());
        //   query.whereContains("to", mFindModelQueryData.getTo());
        //TODO  adjust query based on  number of seats
        //query.whereStartsWith("name", "Zadar");  to

        query.findInBackground(new FindCallback<RideModel>() {
            @Override
            public void done(List<RideModel> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {
                        listOfRides.addAll(objects);
                        Log.d(RoadPalApplication.TAG, "Size of converted list: " + listOfRides.size());

                    mListener.onFindRidesSucess();

                    } else {
                        mListener.onFindRidesNoResults();
                        Log.d(RoadPalApplication.TAG, "0 rides found! ");
                    }
                } else {
                    Log.d(RoadPalApplication.TAG, "Find in background error : " + e.getLocalizedMessage());
                    Log.d(RoadPalApplication.TAG, "getCause: ", e.getCause());
                    mListener.onFindRidesFailure(e);
                }
            }
        });

    }

    @Override
    public ArrayList<LocationPointModel> autocomplete(String constraint) {
        return mPlaceAPI.autocomplete(constraint);

    }


}
