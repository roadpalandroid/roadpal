package com.rootcave.roadpal.ride;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 14.12.2015..
 */
public interface IFindRideView {

    void onFindRideSucess();

    void onFindRideFailure(ParseException e);

    void showProgress();

    void hideProgress();


}
