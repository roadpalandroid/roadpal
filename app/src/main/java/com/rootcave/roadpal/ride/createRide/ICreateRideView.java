package com.rootcave.roadpal.ride.createRide;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 9.12.2015..
 */
public interface ICreateRideView {
    void onProcessedDataError(ParseException e);
    void onProcessedDataSucess();
    void showProgress();
    void hideProgress();
    void navigateToRideDetailActivity();
}
