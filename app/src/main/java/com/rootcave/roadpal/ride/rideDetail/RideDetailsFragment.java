package com.rootcave.roadpal.ride.rideDetail;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.findRide.FoundRidesActivity;
import com.rootcave.roadpal.ride.myRides.MyRidesActivity;
import com.kogitune.activity_transition.fragment.ExitFragmentTransition;
import com.kogitune.activity_transition.fragment.FragmentTransition;
import com.parse.GetFileCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 24.12.2015..
 */
public class RideDetailsFragment extends Fragment implements IRideDetailsView, View.OnClickListener {

    public static String TAG = "RideDetailsFragment";

//TODO  replace edit texts  with textViews coz entire layout is for view only

    @Bind(R.id.image_view_owners_image) ImageView userImageView;
    @Bind(R.id.textView_pending_requests) TextView pendingRequestsTextView;
    @Bind(R.id.textView_user) TextView userTextView;
    @Bind(R.id.user_edit_text) MaterialEditText userEditText;
    @Bind(R.id.from_edit_text) MaterialEditText fromEditText;
    @Bind(R.id.to_edit_text) MaterialEditText toEditText;
    @Bind(R.id.date_edit_text) MaterialEditText dateEditText;
    @Bind(R.id.time_edit_text) MaterialEditText timeEditText;
    @Bind(R.id.for_edit_text) MaterialEditText genderAllowedEditText;
    @Bind(R.id.vehicle_type_edit_text) MaterialEditText vehicleTypeEditText;
    @Bind(R.id.vehicle_plate_number_edit_text) MaterialEditText vehiclePlateNumberEditText;
    @Bind(R.id.available_seats_edit_text) MaterialEditText availableSeatsEditText;
    @Bind(R.id.button_call) Button buttonCall;
    @Bind(R.id.button_join_ride) Button buttonJoinride;

    private String from, to, genderAllowed, vehicleType, vehiclePlate;
    private Integer availableSeats;
    private Date dateSelected= null;
    private RideModel rideModel;
    private IRideDetailPresenter mRideDetailPresenter;

    private Callbacks mCallbacks = sDummyCallbacks;

    public static RideDetailsFragment newInstance()
    {
        RideDetailsFragment fragment= new RideDetailsFragment();
        return fragment;
    }



    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(RideModel rideModel);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(RideModel rideModel) {
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ride_details, container, false);
        ButterKnife.bind(this, view);
        fromEditText.setText(from);
        toEditText.setText(to);
        dateEditText.setText(dateSelected.toString());

        genderAllowedEditText.setText(genderAllowed);
        vehicleTypeEditText.setText(vehicleType);
        vehiclePlateNumberEditText.setText(vehiclePlate);
        availableSeatsEditText.setText(String.valueOf(availableSeats));


        //rideModel= EventBus.getDefault().getStickyEvent(RideModel.class);
        //TODO move logic to interactor
        ParseFile photoImage = rideModel.getRideOwner().get_users_image();
        if (photoImage != null) {
            photoImage.getFileInBackground(new GetFileCallback() {
                @Override
                public void done(File file, ParseException e) {
                    userImageView.setImageDrawable(Drawable.createFromPath(file.getAbsolutePath()));
                }
            });
        } else {
            userImageView.setImageDrawable(RoadPalApplication.getContext().getResources().getDrawable(R.drawable.ic_person));
        }

        //if this is the ride of current logged in user hide User text view and call button
       // if (rideModel.getUserObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
        if ( rideModel.getRideOwner().get_object_id().equals(ParseUser.getCurrentUser().getObjectId())) {

            pendingRequestsTextView.setVisibility(View.VISIBLE);
            if (rideModel.getListOfPendingPassengersId() != null) {
                pendingRequestsTextView.setText(rideModel.getListOfPendingPassengersId().size() + " new requests!");
                pendingRequestsTextView.setOnClickListener(this);
            }
            userTextView.setVisibility(View.INVISIBLE);
            userEditText.setVisibility(View.INVISIBLE);
            buttonCall.setVisibility(View.GONE);
            buttonJoinride.setVisibility(View.GONE);

        } else {
            pendingRequestsTextView.setVisibility(View.GONE);
            userEditText.setText(rideModel.getRideOwner().get_username());
            buttonCall.setOnClickListener(this);
            buttonJoinride.setOnClickListener(this);
        }

        Log.d(RoadPalApplication.TAG, "inside fragmentsonCreateView");

        //execute transitions only in two pane master detail view
        if (getActivity().getClass().equals(MyRidesActivity.class) || getActivity().getClass().equals(FoundRidesActivity.class)) {
            Log.d(RoadPalApplication.TAG, "this fragment was launched by MyRidesActivity");
            final ExitFragmentTransition exitFragmentTransition = FragmentTransition.with(this).to(view.findViewById(R.id.image_view_owners_image)).start(savedInstanceState);
            final ExitFragmentTransition exitFragmentTransition1 = FragmentTransition.with(this).to(view.findViewById(R.id.from_edit_text)).start(savedInstanceState);
            final ExitFragmentTransition exitFragmentTransition2 = FragmentTransition.with(this).to(view.findViewById(R.id.to_edit_text)).start(savedInstanceState);
            final ExitFragmentTransition exitFragmentTransition3 = FragmentTransition.with(this).to(view.findViewById(R.id.time_edit_text)).start(savedInstanceState);

            exitFragmentTransition.startExitListening();
            exitFragmentTransition1.startExitListening();
            exitFragmentTransition2.startExitListening();
            exitFragmentTransition3.startExitListening();
        }


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    public void setRideModel(RideModel model) {

        rideModel = model;
        from = rideModel.getFrom();
        to = rideModel.getTo();
        dateSelected = rideModel.getDate();
        genderAllowed = rideModel.getGenderAllowed();
        vehicleType = rideModel.getVehicleType();
        vehiclePlate = rideModel.getPlateNumber();
        availableSeats = rideModel.getAvailableSeats();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.textView_pending_requests:
                mCallbacks.onItemSelected(rideModel);
                Log.d(RoadPalApplication.TAG, "Pending requests clicked!");
                break;
            case R.id.button_join_ride:
                mRideDetailPresenter = new RideDetailPresenterImpl(this);
                mRideDetailPresenter.processJoinRide(rideModel);
                break;
            case R.id.button_call:
                Log.d(RoadPalApplication.TAG, "Button Call clicked!");
                break;
        }
    }

    @Override
    public void onJoinRideRequestSentSucess() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
