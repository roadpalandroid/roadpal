package com.rootcave.roadpal.ride.rideDetail;

/**
 * Created by Lenovo T420 on 28.12.2015..
 */
public interface IRideDetailsView {

    void onJoinRideRequestSentSucess();

    void showProgress();

    void hideProgress();


}
