package com.rootcave.roadpal.ride;


import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.parse.GetFileCallback;
import com.parse.ParseException;
import com.parse.ParseFile;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RideModelRecycleViewAdapter extends RecyclerView.Adapter<RideModelRecycleViewAdapter.ViewHolder> {

    private ArrayList<RideModel> listOfRideModels;
    private ArrayList<String> listOfPendingPassenegers;
    //private Activity activity;

    public RideModelRecycleViewAdapter(ArrayList<RideModel> rideModels/*, Activity activity*/) {
        listOfRideModels = rideModels;
        // this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RideModelRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_found_rides, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        RideModel currentRideModel = listOfRideModels.get(position);

        //Access data using the get methods for the object

        Log.d(RoadPalApplication.TAG, "position: " + position);
        //happens if data hasnt been yet fetched asynchrounosly
        if (currentRideModel.getRideOwner() != null) {
            Log.d(RoadPalApplication.TAG, "currentride model.get ride owner.get username: " + currentRideModel.getRideOwner().get_username());
            String usernameRideOwner = currentRideModel.getRideOwner().get_username();
            holder.textViewOwner.setText(usernameRideOwner);
            ParseFile photoImage = currentRideModel.getRideOwner().get_users_image();
            if (photoImage != null) {
                photoImage.getFileInBackground(new GetFileCallback() {
                    @Override
                    public void done(File file, ParseException e) {
                        if (e == null) {
                            Log.d(RoadPalApplication.TAG, "file.getAbsolutePath(): " + file.getAbsolutePath());
                            holder.imageViewOwner.setImageDrawable(Drawable.createFromPath(file.getAbsolutePath()));
                        } else {
                            Log.d(RoadPalApplication.TAG, "RideModelRecycleViewAdapter error on retrieving users image: " + e.getLocalizedMessage());

                        }
                    }
                });
            } else {
                holder.imageViewOwner.setImageDrawable(RoadPalApplication.getContext().getResources().getDrawable(R.drawable.ic_person));
            }
        }

        holder.textViewFrom.setText(currentRideModel.getFrom());
        holder.textViewTo.setText(currentRideModel.getTo());
        holder.textViewDate.setText(currentRideModel.getDate().toString());


        holder.textViewTravelTime.setText(currentRideModel.getTravelTime());
        holder.textViewDistance.setText(currentRideModel.getdistanceKm());

        // holder.textViewTime.setText(currentRideModel.getTime());

        holder.textViewTitlePendingJoinRideRequest.setVisibility(View.GONE);
        holder.textViewPendingJoinRideRequest.setVisibility(View.GONE);
        //if this is the ride from current logged in user

        Log.d(RoadPalApplication.TAG, "Ride model recycle view adapter:currentRideModel.getUserObjectId(): " + currentRideModel.getRideOwner().get_object_id());

        if (currentRideModel.getRideOwner().get_object_id().equals(ParseUser.getCurrentUser().getObjectId())) {
            listOfPendingPassenegers = currentRideModel.getListOfPendingPassengersId();
            if (listOfPendingPassenegers != null) {
                holder.textViewTitlePendingJoinRideRequest.setVisibility(View.VISIBLE);
                holder.textViewPendingJoinRideRequest.setVisibility(View.VISIBLE);
                holder.textViewPendingJoinRideRequest.setText(listOfPendingPassenegers.size() + " new pending requests!");
            }
        } else
        {
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (listOfRideModels != null) {
            return listOfRideModels.size();
        } else
            return 0;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, AdapterView.OnItemClickListener {

        @Bind(R.id.image_view_owners_image)
        ImageView imageViewOwner;
        @Bind(R.id.textView_owner)
        TextView textViewOwner;
        @Bind(R.id.text_view_from)
        TextView textViewFrom;
        @Bind(R.id.text_view_to)
        TextView textViewTo;
        @Bind(R.id.text_view_date)
        TextView textViewDate;
        @Bind(R.id.text_view_distance)
        TextView textViewDistance;
        @Bind(R.id.text_view_travel_time)
        TextView textViewTravelTime;

        @Bind(R.id.title_pending_join_ride_request)
        TextView textViewTitlePendingJoinRideRequest;
        @Bind(R.id.pending_join_ride_request)
        TextView textViewPendingJoinRideRequest;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnCreateContextMenuListener(this);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* int position = getPosition();
                    Intent intent = new Intent(_activity, RideDetailsActivity.class);
                    String objectId = listOfRideModels.get(position).getObjectId();
                    intent.putExtra("objectId", objectId);
                    _activity.startActivity(intent);*/
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {


        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Log.d(RoadPalApplication.TAG, "Item on position: " + position + "clicked!");

        }
    }


}


