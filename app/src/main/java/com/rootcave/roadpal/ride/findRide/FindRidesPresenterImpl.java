package com.rootcave.roadpal.ride.findRide;

import com.rootcave.roadpal.ride.IFindRideView;
import com.rootcave.roadpal.ride.IonFindRidesFinishedListener;
import com.rootcave.roadpal.ride.RideModel;
import com.parse.ParseException;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 14.12.2015..
 */
public class FindRidesPresenterImpl implements IFindRidePresenter, IonFindRidesFinishedListener {
    private IFindRideInteractor mFindRideInteractor;
    private IFindRideView mFindRideView;


    public FindRidesPresenterImpl(IFindRideView findRideView) {
        mFindRideInteractor = new FindRideInteractorImpl(this);
        mFindRideView = findRideView;
    }

    //method from IFindRidePresenter
    @Override
    public void processInputData(RideModel findModelQueryData, ArrayList<RideModel> listOfRides) {
        mFindRideInteractor.processInputData(findModelQueryData, listOfRides);
        mFindRideView.showProgress();
    }

    @Override
    public ArrayList<LocationPointModel> autocomplete(String constraint) {
       return  mFindRideInteractor.autocomplete(constraint);
    }


    //Methods from IonFindRidesFinishedListener
    @Override
    public void onFindRidesSucess() {
        mFindRideView.onFindRideSucess();
        mFindRideView.hideProgress();
    }

    @Override
    public void onFindRidesNoResults() {
        mFindRideView.hideProgress();
    }

    @Override
    public void onFindRidesFailure(ParseException e) {
        mFindRideView.hideProgress();
    }
}

