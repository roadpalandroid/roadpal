package com.rootcave.roadpal.ride.myRides.pendingUsers;

import com.parse.ParseException;
import com.rootcave.roadpal.common.ParseUserModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 27.1.2016..
 */
public interface IPendingUsersListView {
    void onFetchUsersDataSucess();

    void onFetchUsersDataFailure(ParseException e);

    void onProcessPendingPassengerSucess();
    void onProcessPendingPassengerFailure(ParseException e);

    void showProgress();

    void hideProgress();
}
