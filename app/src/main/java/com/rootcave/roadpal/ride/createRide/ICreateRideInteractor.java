package com.rootcave.roadpal.ride.createRide;

import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 9.12.2015..
 */
public interface ICreateRideInteractor {

   void validateInputData(RideModel rideModel);
    ArrayList<LocationPointModel> autocomplete(String constraint);
    void fetchGeoPoints(LocationPointModel locationPoint);


}
