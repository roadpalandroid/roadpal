package com.rootcave.roadpal.ride.myRides.pendingUsers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.LifecycleLoggingActivity;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;

import de.greenrobot.event.EventBus;

/**
 * Created by Lenovo T420 on 26.1.2016..
 */
public class PendingRequestsActivity extends LifecycleLoggingActivity implements  PendingUserListFragment.Callbacks {
    private PendingUserListFragment mFragment;
    private Toolbar toolbar;
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_requests);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final FragmentManager fm = getSupportFragmentManager();
        mFragment = (PendingUserListFragment) fm.findFragmentByTag(PendingUserListFragment.TAG);

        if (mFragment == null) {
            mFragment = new PendingUserListFragment();
            RideModel model = EventBus.getDefault().getStickyEvent(RideModel.class);
            mFragment.setRideModel(model);
            Log.d(RoadPalApplication.TAG, "new PendingUserListFragment created");

            fm.beginTransaction().add(R.id.frag_user_list_container, mFragment, PendingUserListFragment.TAG).commit();

            /*
            if (findViewById(R.id.frag_user_detail_container) != null) {
                // The detail container view will be present only in the
                // large-screen layouts (res/values-large and
                // res/values-sw600dp). If this view is present, then the
                // activity should be in two-pane mode.
                mTwoPane = true;
                Log.d(RoadPalApplication.TAG, "Mtwopane == true");
            }
            */
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            // in case of small screens this activity is called from FoundRidesActivity and from MyRidesActivity
            finish();
          /*  NavUtils.navigateUpTo(this,
                    new Intent(this, MyRidesActivity.class));*/
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemSelected(ParseUserModel model, View view) {

    launchPendingUserDetailActivity(model);

    }

    private void launchPendingUserDetailActivity(ParseUserModel model)
    {
        EventBus.getDefault().postSticky(model);
        Intent userIntent= new Intent(this, PendingUserDetailsActivity.class);
        startActivity(userIntent);
        Log.d(RoadPalApplication.TAG, "Pending User Details Activity launched from Pending requests activity ! ");




    }
}
