package com.rootcave.roadpal.ride.myRides.pendingUsers;

import com.parse.ParseException;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 27.1.2016..
 */
public class PendingUserPresenterImpl implements IPendingUserPresenter, IonProcessPendingUsersDataFinishedListener {
    private IPendingUsersListView mUsersListView;
    private IPendingUserInteractor mUserInteractor;

    public PendingUserPresenterImpl(IPendingUsersListView usersListView) {
        mUsersListView = usersListView;
        this.mUserInteractor = new PendingUserInteractorImpl(this);
    }

    @Override
    public void fetchUsersData(RideModel listOfPendingPassengersIds, ArrayList<ParseUserModel> listOfPendingPassengers) {
        mUserInteractor.extractUsersData(listOfPendingPassengersIds, listOfPendingPassengers);
        mUsersListView.showProgress();

    }

    @Override
    public void acceptPendingPassenger(int position) {
        mUserInteractor.acceptPendingPassenger(position);
        mUsersListView.showProgress();

    }

    @Override
    public void declinePendingPassenger(int position) {
        mUserInteractor.declinePendingPassenger(position);
        mUsersListView.showProgress();
    }

    @Override
    public void onProcessUsersDataSucess() {
        mUsersListView.onFetchUsersDataSucess();
        mUsersListView.hideProgress();
    }


    @Override
    public void onProcessUsersDataFailure(ParseException e) {
        mUsersListView.onFetchUsersDataFailure(e);
        mUsersListView.hideProgress();

    }

    @Override
    public void onProcessPendingPassengerSucess() {
        mUsersListView.onProcessPendingPassengerSucess();
        mUsersListView.hideProgress();
    }

    @Override
    public void onProcessPendingPassengerFailure(ParseException e) {
        mUsersListView.onProcessPendingPassengerFailure(e);
        mUsersListView.hideProgress();
    }
}
