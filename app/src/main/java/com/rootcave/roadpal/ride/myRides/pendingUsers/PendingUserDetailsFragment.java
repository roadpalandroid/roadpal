package com.rootcave.roadpal.ride.myRides.pendingUsers;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.parse.GetFileCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 25.1.2016..
 */
public class PendingUserDetailsFragment extends Fragment {
    public static String TAG = "PendingUserDetailsFragment";
    private ParseUserModel userModel;
    private Drawable usersDrawable;

    @Bind(R.id.image_view_users_image)
    ImageView userImageView;
    @Bind(R.id.user_edit_text)
    MaterialEditText userEditText;
    @Bind(R.id.email_edit_text)
    MaterialEditText emailEditText;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // Log.d(RoadPalApplication.TAG, "Drawable from model: " + userModel.getUsersImageDrawable().toString());
        ParseFile photoImage = userModel.get_users_image();
        if (photoImage != null) {
            Log.d(RoadPalApplication.TAG, "ParseFile from model: " + userModel.get_users_image().toString());
        }

        if (photoImage != null) {
            photoImage.getFileInBackground(new GetFileCallback() {
                @Override
                public void done(File file, ParseException e) {
                    if (e == null) {
                        Log.d(RoadPalApplication.TAG, "file.getAbsolutePath(): " + file.getAbsolutePath());
                        usersDrawable = Drawable.createFromPath(file.getAbsolutePath());

                    } else {
                        Log.d(RoadPalApplication.TAG, "PendingUserDetailsFragment error on retrieving users image: " + e.getLocalizedMessage());
                    }
                }
            });
        } else {
            usersDrawable = RoadPalApplication.getContext().getResources().getDrawable(R.drawable.ic_person);
        }


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_details, container, false);
        ButterKnife.bind(this, view);
        userImageView.setImageDrawable(usersDrawable);
        userEditText.setText(userModel.get_full_name());
        emailEditText.setText(userModel.get_email());

        return view;
    }


    public void setUserModel(ParseUserModel userModel) {
        this.userModel = userModel;
    }
}
