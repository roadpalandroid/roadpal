package com.rootcave.roadpal.ride.myRides;

import android.util.Log;

import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.IonFindRidesFinishedListener;
import com.rootcave.roadpal.ride.RideModel;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Lenovo T420 on 17.12.2015..
 */
public class MyRidesInteractorImpl implements IMyRidesInteractor {

    private IonFindRidesFinishedListener mListener;
    private ParseUserModel currentUserModel;

    //TODO refactor whole class
    public MyRidesInteractorImpl(IonFindRidesFinishedListener listener) {
        mListener = listener;
        //TODO check for possibility when user logs out that it wont compare current User to old current user
        currentUserModel= RoadPalApplication.getCurrentUser();

    }

    @Override
    public void fetchMyHostedRides(final ArrayList<RideModel> listOfRides) {


        Log.d(RoadPalApplication.TAG, "fetchMyHostedRides get vehicle model: " + currentUserModel.get_vehicle_model());
        Log.d(RoadPalApplication.TAG, "fetchMyHostedRides currentUserModel.get_object_id():  " + currentUserModel.get_object_id());
        if (currentUserModel.get_vehicle_model() == "") {}
        ParseQuery<RideModel> query = ParseQuery.getQuery(RideModel.class);
        query.whereEqualTo("userObjectId", currentUserModel.get_object_id());
        query.findInBackground(new FindCallback<RideModel>() {
            @Override
            public void done(List<RideModel> objects, ParseException e) {
                if (e == null) {
                            Log.d(RoadPalApplication.TAG, "Size of list: " + objects.size());

                            if (objects.size() > 0) {
                                listOfRides.addAll(objects);
                                Log.d(RoadPalApplication.TAG, "Size of converted list: " + listOfRides.size());
                                Log.d(RoadPalApplication.TAG, "listOfRides.hashCode: " + listOfRides.hashCode());
                                // RideOwnerUtils.getRideOwners(listOfRides, mListener);
                                mListener.onFindRidesSucess();
                    }
                            else {
                                mListener.onFindRidesNoResults();
                                Log.d(RoadPalApplication.TAG, " fetchMyHostedRides 0 rides found! ");
                    }

                } else {
                    Log.d(RoadPalApplication.TAG, "Find in background error : " + e.getLocalizedMessage());
                    Log.d(RoadPalApplication.TAG, "getCause: ", e.getCause());
                    mListener.onFindRidesFailure(e);
                }
            }
        });
    }

    @Override
    public void fetchAcceptedRides(final ArrayList<RideModel> listOfRides) {

        Date currentDate = new Date();
        Log.d(RoadPalApplication.TAG, "current date: " + currentDate);
        Log.d(RoadPalApplication.TAG, "get vehicle model: " + currentUserModel.get_vehicle_model());

        ParseQuery<RideModel> query = ParseQuery.getQuery(RideModel.class);
        query.whereGreaterThan("date",currentDate);
        query.whereEqualTo("listOfAcceptedPassengers", currentUserModel.get_object_id());
        query.findInBackground(new FindCallback<RideModel>() {
            @Override
            public void done(List<RideModel> objects, ParseException e) {
                if (e == null) {
                    Log.d(RoadPalApplication.TAG, "Size of list: " + objects.size());
                        if (objects.size() > 0) {
                            listOfRides.addAll(objects);
                            Log.d(RoadPalApplication.TAG, "Size of converted list: " + listOfRides.size());
                            Log.d(RoadPalApplication.TAG, "listOfRides.hashCode: " + listOfRides.hashCode());
                            mListener.onFindRidesSucess();
                        }
                        else {
                            mListener.onFindRidesNoResults();
                            Log.d(RoadPalApplication.TAG, "0 rides found! ");
                    }

                } else {
                    Log.d(RoadPalApplication.TAG, "Find in background error : " + e.getLocalizedMessage());
                    Log.d(RoadPalApplication.TAG, "getCause: ", e.getCause());
                    mListener.onFindRidesFailure(e);
                }
            }
        });
    }

    @Override
    public void fetchPendingRides(final ArrayList<RideModel> listOfRides) {
        Log.d(RoadPalApplication.TAG, "get vehicle model: " + currentUserModel.get_vehicle_model());

        if (currentUserModel.get_vehicle_model() == "") {}
        ParseQuery<RideModel> query = ParseQuery.getQuery(RideModel.class);
        query.whereEqualTo("listOfPendingPassengers", currentUserModel.get_object_id());
        query.findInBackground(new FindCallback<RideModel>() {
            @Override
            public void done(List<RideModel> objects, ParseException e) {
                if (e == null) {
                    Log.d(RoadPalApplication.TAG, "Size of list: " + objects.size());
                    if (objects.size() > 0) {
                        listOfRides.addAll(objects);
                        Log.d(RoadPalApplication.TAG, "Size of converted list: " + listOfRides.size());
                        Log.d(RoadPalApplication.TAG, "listOfRides.hashCode: " + listOfRides.hashCode());
                        mListener.onFindRidesSucess();
                    }
                    else {
                        mListener.onFindRidesNoResults();
                        Log.d(RoadPalApplication.TAG, "0 rides found! ");
                    }
                }
                else {
                    Log.d(RoadPalApplication.TAG, "Find in background error : " + e.getLocalizedMessage());
                    Log.d(RoadPalApplication.TAG, "getCause: ", e.getCause());
                    mListener.onFindRidesFailure(e);
                }
            }
        });

    }


}