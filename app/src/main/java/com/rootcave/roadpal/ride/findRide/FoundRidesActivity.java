package com.rootcave.roadpal.ride.findRide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.RideModelList;
import com.rootcave.roadpal.ride.myRides.pendingUsers.PendingRequestsActivity;
import com.rootcave.roadpal.ride.rideDetail.RideDetailActivity;
import com.rootcave.roadpal.ride.rideDetail.RideDetailsFragment;
import com.kogitune.activity_transition.fragment.FragmentTransitionLauncher;

import de.greenrobot.event.EventBus;

/**
 * Created by Lenovo T420 on 6.1.2016..
 */
public class FoundRidesActivity extends BaseActivity implements FoundRidesListFragment.Callbacks,RideDetailsFragment.Callbacks{

    private FoundRidesListFragment mFragment;
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found_rides);
        super.onCreateToolbar();

        FragmentManager fm = getSupportFragmentManager();
        mFragment = (FoundRidesListFragment) fm.findFragmentByTag(FoundRidesListFragment.TAG);
        if (mFragment == null) {
            mFragment = new FoundRidesListFragment();
            RideModelList mlist= EventBus.getDefault().getStickyEvent(RideModelList.class);
            mFragment.setRideModelList(mlist.getListOfRides());

            Log.d(RoadPalApplication.TAG, "new FoundRidesListFragment created");
            fm.beginTransaction().add(R.id.frag_list_container, mFragment, FoundRidesListFragment.TAG).commit();
        }

        if (findViewById(R.id.frag_item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
            Log.d(RoadPalApplication.TAG, "Mtwopane == true");
        }


    }

    //from FoundRidesListFragment.Callbacks
    @Override
    public void onItemSelected(RideModel model,View view) {
        Log.d(RoadPalApplication.TAG, "Item clicked: " + model.getRideOwner().get_username());
        if(mTwoPane)
        {
            launchRideDetailsFragment(model,view);
        }
        else
        {
           launchRideDetailActivity(model, view);
        }
    }
// from RideDetailsFragment.Callbacks
    @Override
    public void onItemSelected(RideModel rideModel) {
        launchPendingRequestsActivity(rideModel);
        Log.d(RoadPalApplication.TAG, "Pending request activity launched from FoundRidesActivity (twopane)! ");
    }

    private void launchRideDetailsFragment(RideModel model,View view) {

        RideDetailsFragment detailFragment = new RideDetailsFragment();
        detailFragment.setRideModel(model);

        FragmentTransitionLauncher
                .with(view.getContext())
                        //.image(BitmapFactory.decodeResource(getResources(), R.id.image_view_owners_image))
                .from(view.findViewById(R.id.image_view_owners_image)).prepare(detailFragment);

        getSupportFragmentManager().beginTransaction().replace(R.id.frag_item_detail_container, detailFragment, RideDetailsFragment.TAG).commit();
    }
    private void launchRideDetailActivity(RideModel model,View view) {

        // In single-pane mode, simply start the detail activity
        //sends RideModel to RideDetailActivity via EventBus
        //implemented this way because RideModel which inherits from ParseObject cannot be serialized or parcelized
        EventBus.getDefault().postSticky(model);
        Intent detailIntent = new Intent(this, RideDetailActivity.class);
        Log.d(RoadPalApplication.TAG, "model.get_ObjectId(): " + model.get_ObjectId());
        startActivity(detailIntent);
    }

    private void launchPendingRequestsActivity(RideModel model) {
        EventBus.getDefault().postSticky(model);
        Intent userIntent= new Intent(this, PendingRequestsActivity.class);
        startActivity(userIntent);
        Log.d(RoadPalApplication.TAG, "Pending request activity launched from ride detail activity! ");

    }


}
