package com.rootcave.roadpal.ride.myRides;

import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 17.12.2015..
 */
public interface IMyRidesInteractor {



    void fetchMyHostedRides(ArrayList<RideModel> listOfRides);

    void fetchAcceptedRides(ArrayList<RideModel> listOfRides);
    void fetchPendingRides(ArrayList<RideModel> listOfRides);
}
