package com.rootcave.roadpal.ride.rideDetail;

import com.rootcave.roadpal.ride.RideModel;

/**
 * Created by Lenovo T420 on 28.12.2015..
 */
public interface IRideDetailInteractor {

    void sendJoinRideRequest(RideModel rideModel);
}
