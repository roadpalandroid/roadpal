package com.rootcave.roadpal.ride.rideDetail;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 28.12.2015..
 */
public interface IonProcessJoinRideFinishedListener {
    void onProcessJoinRideSucess();
    void onProcessJoinRideFailure(ParseException e);
}
