package com.rootcave.roadpal.ride.createRide;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by Lenovo T420 on 1.3.2016..
 */
@ParseClassName("LocationPoint")
public class LocationPointModel extends ParseObject {


    public LocationPointModel() {
    }


    public String getPlace_id(){return getString("place_id");}
    public String getAdress_description(){return getString("adress_description");}
    public String getTypes(){return getString("types");}
    public ParseGeoPoint getPoint_coordinates(){return getParseGeoPoint("point_coordinates");}


    public void setPlaceId(String place_id) {put("place_id",place_id);}
    public void setAdress_description(String adress_description){put("adress_description",adress_description);}
    public void setTypes(String types){put("types",types);}
    public void setPointCoordinates(ParseGeoPoint point_coordinates ){put("point_coordinates",point_coordinates);}






}
