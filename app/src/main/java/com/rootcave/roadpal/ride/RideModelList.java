package com.rootcave.roadpal.ride;

import java.util.ArrayList;

/**
 * Class used to populate and send data with event bus post sticky from FindRide activity to FoundRidesActivity
 */
public class RideModelList {

    private ArrayList<RideModel> listOfRides;


    public ArrayList<RideModel> getListOfRides() {
        return listOfRides;
    }

    public void setListOfRides(ArrayList<RideModel> listOfRides) {
        this.listOfRides = listOfRides;
    }
}
