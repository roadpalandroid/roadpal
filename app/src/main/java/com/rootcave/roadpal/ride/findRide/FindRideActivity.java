package com.rootcave.roadpal.ride.findRide;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.parse.ParseException;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.ride.PlacesAutoCompleteAdapter;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.IFindRideView;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.RideModelList;
import com.rootcave.roadpal.ride.customViews.FancyCoverFlow;
import com.rootcave.roadpal.ride.customViews.ViewGroupExampleAdapter;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by Lenovo T420 on 4.12.2015..
 */
public class FindRideActivity extends BaseActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, View.OnClickListener, IFindRideView {


    @Bind(R.id.fancyCoverFlow)
    FancyCoverFlow fancyCoverFlow;
    @Bind(R.id.autocomplete_text_view_from)
    AutoCompleteTextView autoCompleteTextViewFrom;
    @Bind(R.id.autocomplete_text_view_to)
    AutoCompleteTextView autoCompleteTextViewTo;
    @Bind(R.id.date_edit_text)
    MaterialEditText dateEditText;
    @Bind(R.id.time_edit_text)
    MaterialEditText timeEditText;
    @Bind(R.id.available_seats_edit_text)
    MaterialEditText availableSeatsEditText;
    @Bind(R.id.button_find)
    Button buttonFind;

    private String from;
    private String to;
    private String date;
    private String time;
    private String genderAllowed;
    private int availableSeats;
    private ArrayList<RideModel> listOfRides = new ArrayList<RideModel>();
    private IFindRidePresenter mFindRidePresenter;
    private RideModel mFindModelQueryData;
    private ProgressDialog progressDialog;
    private FoundRidesListFragment mFragment;
    private Date dateSelected= null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_ride);
        super.onCreateToolbar();

        ButterKnife.bind(this);
        mFindRidePresenter = new FindRidesPresenterImpl(this);
        fancyCoverFlow.setAdapter(new ViewGroupExampleAdapter());
        autoCompleteTextViewFrom.setAdapter(new PlacesAutoCompleteAdapter(this,mFindRidePresenter));
        autoCompleteTextViewTo.setAdapter(new PlacesAutoCompleteAdapter(this,mFindRidePresenter));

        dateEditText.setOnClickListener(this);
        timeEditText.setOnClickListener(this);
        buttonFind.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.date_edit_text:
                createDatePickerDialog();
                break;

            case R.id.time_edit_text:
                createTimePickerDialog();
                break;
            case R.id.button_find:

                from = autoCompleteTextViewFrom.getText().toString().trim();
                to = autoCompleteTextViewTo.getText().toString().trim();
                genderAllowed = fancyCoverFlow.getSelectedItem().toString();
                availableSeats = Integer.valueOf(availableSeatsEditText.getText().toString().trim());

                mFindModelQueryData = new RideModel();
                mFindModelQueryData.setFrom(from);
                mFindModelQueryData.setTo(to);
                mFindModelQueryData.setDate(dateSelected);
                mFindModelQueryData.setGenderAllowed(genderAllowed);
                mFindModelQueryData.setAvailableSeats(availableSeats);

                mFindRidePresenter.processInputData(mFindModelQueryData, listOfRides);
                break;
        }
    }

    private void createDatePickerDialog() {
        Log.d(RoadPalApplication.TAG, "date picker edit text listener! ");
        Calendar date = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                FindRideActivity.this,
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH)
        );
        dpd.showYearPickerFirst(false);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private void createTimePickerDialog() {
        Calendar time = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                FindRideActivity.this,
                time.get(Calendar.HOUR_OF_DAY),
                time.get(Calendar.MINUTE), true
        );
        tpd.setTitle("TimePicker Title");
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        Log.d(RoadPalApplication.TAG,"year: "+year);
        dateSelected= new Date();
        dateSelected.setYear(year - 1900);
        dateSelected.setMonth(monthOfYear);
        dateSelected.setDate(dayOfMonth);
        String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        dateEditText.setText(date);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

        Log.d(RoadPalApplication.TAG,"hour ofr day: "+hourOfDay);

        TimeZone tz = TimeZone.getDefault();
        int gmtTimezone = (int) TimeUnit.HOURS.convert(tz.getRawOffset(), TimeUnit.MILLISECONDS);

        if (dateSelected == null) {
            dateSelected = new Date();
        }
        dateSelected.setHours(hourOfDay+gmtTimezone);
        dateSelected.setMinutes(minute);
        Log.d(RoadPalApplication.TAG, "date selected: " + dateSelected);
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = hourString + "h" + minuteString + "m" + secondString + "s";
        timeEditText.setText(time);
    }


    @Override
    public void onFindRideSucess() {
        launchFoundRidesActivity();
    }

    @Override
    public void onFindRideFailure(ParseException e) {
        Log.d(RoadPalApplication.TAG,"Error happened: "+e.getLocalizedMessage());
    }

    @Override
    public void showProgress() {
        //TODO implement progress bar
        progressDialog = new ProgressDialog(FindRideActivity.this);
        progressDialog.setMessage(getString(R.string.progress_search_rides));
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        //TODO implement progress bar
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void launchFoundRidesActivity() {
        RideModelList mRideModelList= new RideModelList();
        mRideModelList.setListOfRides(listOfRides);
        //RideModel extends ParseObject and  cannot be serialized or parecelized so im using event bus
        EventBus.getDefault().postSticky(mRideModelList);
        Intent foundRidesIntent = new Intent(this, FoundRidesActivity.class);
        startActivity(foundRidesIntent);


      /*
        mFragment = new FoundRidesListFragment();
        mFragment.setRideModelList(listOfRides);
        setContentView(R.layout.fragment_container);
        getSupportFragmentManager().beginTransaction().add(R.id.frag_container, mFragment,FoundRidesListFragment.TAG).commit();
        */
    }
}
