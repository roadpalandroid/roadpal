package com.rootcave.roadpal.ride;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 14.12.2015..
 */
public interface IonFindRidesFinishedListener {

    void onFindRidesSucess();
    void onFindRidesNoResults();
    void onFindRidesFailure(ParseException e);

}
