package com.rootcave.roadpal.ride.createRide;

import com.rootcave.roadpal.ride.RideModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 9.12.2015..
 */
public class CreateRidePresenterImpl implements ICreateRidePresenter, IonCreateRideFinishedListener {

    private ICreateRideInteractor mCreateRideInteractor;
    private ICreateRideView mCreateRideView;

    public CreateRidePresenterImpl(ICreateRideView createRideView) {

        this.mCreateRideInteractor = new CreateRideInteractorImpl(this);
        mCreateRideView = createRideView;
    }

    @Override
    public void processInputData(RideModel rideModel) {
        mCreateRideInteractor.validateInputData(rideModel);

    }

    @Override
    public void fetchGeoPoints(LocationPointModel locationPoint) {
        mCreateRideInteractor.fetchGeoPoints(locationPoint);
    }

    @Override
    public void onCreateRideValidationSucess() {

    }

    @Override
    public void onCreateRideValidationFailure() {

    }

    @Override
    public void onSaveModelToParseSucess() {

    }

    @Override
    public void onSaveModelToParseFailure() {

    }


    @Override
    public ArrayList<LocationPointModel> autocomplete(String constraint) {
        return mCreateRideInteractor.autocomplete(constraint);
    }
}
