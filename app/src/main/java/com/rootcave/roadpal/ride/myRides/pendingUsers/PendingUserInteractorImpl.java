package com.rootcave.roadpal.ride.myRides.pendingUsers;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 27.1.2016..
 */
public class PendingUserInteractorImpl implements IPendingUserInteractor {

    private IonProcessPendingUsersDataFinishedListener mListener;
    private ArrayList<ParseUserModel> mListOfPendingPassengers;
    private ArrayList<String> listOfPendingPassengersIds;
    private RideModel mRideModel;

    public PendingUserInteractorImpl(IonProcessPendingUsersDataFinishedListener listener) {
        mListener = listener;
    }


    @Override
    public void extractUsersData(RideModel rideModel, ArrayList<ParseUserModel> listOfPendingPassengers) {
        mRideModel = rideModel;
        mListOfPendingPassengers = listOfPendingPassengers;

        listOfPendingPassengersIds = mRideModel.getListOfPendingPassengersId();
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereContainedIn("objectId", listOfPendingPassengersIds);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> listOfParseUsers, ParseException e) {
                if (e != null) {
                    mListener.onProcessUsersDataFailure(e);
                    Log.d(RoadPalApplication.TAG, "remote Find users error: " + e.getLocalizedMessage());
                    Log.d(RoadPalApplication.TAG, "remote Find users error code: " + e.getCode());
                } else {
                    Log.d(RoadPalApplication.TAG, "Fetched " + listOfParseUsers.size() + " users from network!");

                    // listOfPendingPassengers = new ArrayList<ParseUserModel>();
                    for (ParseUser userObject : listOfParseUsers) {
                        ParseUserModel userModel = new ParseUserModel(userObject);
                        mListOfPendingPassengers.add(userModel);
                    }
                    Log.d(RoadPalApplication.TAG, "succesfully fetched all users from network! ");
                    mListener.onProcessUsersDataSucess();
                }
            }
        });


    }

    @Override
    public void acceptPendingPassenger(int position) {

        final String pendingPassengerId = listOfPendingPassengersIds.get(position);
        mRideModel.setAcceptedPassenger(pendingPassengerId);
        mListOfPendingPassengers.remove(position);
        listOfPendingPassengersIds.remove(position);
        mRideModel.setListOfPendingPassengersId(listOfPendingPassengersIds);

        mRideModel.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    mListener.onProcessPendingPassengerFailure(e);
                } else {
                    Log.d(RoadPalApplication.TAG, "succesfully removed accepted user from pending passengers and added to acceptedPassengers! ");
                    sendPushNotifications(pendingPassengerId);
                    mListener.onProcessPendingPassengerSucess();
                }

            }
        });
    }

    @Override
    public void declinePendingPassenger(int position) {
        //TODO  notify user his request to join ride has been rejected
        mListOfPendingPassengers.remove(position);
        listOfPendingPassengersIds.remove(position);
        mRideModel.setListOfPendingPassengersId(listOfPendingPassengersIds);
        mRideModel.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                if (e != null) {
                    mListener.onProcessPendingPassengerFailure(e);
                } else {
                    Log.d(RoadPalApplication.TAG, "succesfully removed  user from pending passengers  ");
                    mListener.onProcessPendingPassengerSucess();
                }
            }
        });
    }


    private void sendPushNotifications(String acceptedPassengerId) {

        ParseQuery<ParseInstallation> query = ParseInstallation.getQuery();
        //query.whereContainedIn("userId", Arrays.asList(user));
        query.whereContains("userId", acceptedPassengerId);
        // send push notification
        ParsePush push = new ParsePush();
        push.setQuery(query);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", "RoadPal");
            jsonObject.put("alert", "User " + ParseUser.getCurrentUser().getUsername() + " has accepted your request to join ride!");
            jsonObject.put("sender", "AcceptPassengerNotification");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        push.setData(jsonObject);
        push.sendInBackground();
        Log.d(RoadPalApplication.TAG, "Push should be sent");
    }

    // }
}
