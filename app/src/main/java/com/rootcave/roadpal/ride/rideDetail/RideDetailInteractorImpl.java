package com.rootcave.roadpal.ride.rideDetail;

import android.util.Log;

import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RideModel;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lenovo T420 on 28.12.2015..
 */
public class RideDetailInteractorImpl implements IRideDetailInteractor {

    IonProcessJoinRideFinishedListener mListener;
    public RideDetailInteractorImpl(IonProcessJoinRideFinishedListener listener) {
        mListener=listener;
    }


    @Override
    public void sendJoinRideRequest(final RideModel rideModel) {
        ParseUserModel currentUser = new ParseUserModel(ParseUser.getCurrentUser());

        // Log.d(RoadPalApplication.TAG, "size of list: "+rideModel.getListOfPendingPassengersId().size()) ;
        rideModel.setPassengerRequest(currentUser.get_object_id());
        rideModel.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {

                    Log.d(RoadPalApplication.TAG, "succesfully updated  list of pending passengers");
                  sendPushNotifications(rideModel);
                    // rideModel.g
                } else {
                    Log.d(RoadPalApplication.TAG, "Error happened while saving list of passengers");
                }
            }
        });
    }

    private void sendPushNotifications(RideModel rideModel) {
    ParseUserModel  rideOwnerUser=rideModel.getRideOwner();

        ParseQuery<ParseInstallation> query = ParseInstallation.getQuery();

        //query.whereContainedIn("userId", Arrays.asList(user));
        query.whereContains("userId", rideOwnerUser.get_object_id());
        // send push notification
        if(rideOwnerUser.get_push_enabled()) {
            ParsePush push = new ParsePush();
            push.setQuery(query);
           // push.setMessage("User " + ParseUser.getCurrentUser().getUsername() + " wants to ride with you!");
            JSONObject jsonObject= new JSONObject();
            try {
                jsonObject.put("title","RoadPal");
                jsonObject.put("alert","User " + ParseUser.getCurrentUser().getUsername() + " wants to ride with you!");
                jsonObject.put("sender","PendingNotification");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            push.setData(jsonObject);
            push.sendInBackground();
            Log.d(RoadPalApplication.TAG, "UserId: "+rideOwnerUser.get_object_id());
            Log.d(RoadPalApplication.TAG, "Push should be sent");
        }

    }








}
