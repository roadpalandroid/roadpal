package com.rootcave.roadpal.ride.myRides.pendingUsers;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.GetFileCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 26.1.2016..
 */
public class PendingUserListRecycleViewAdapter extends RecyclerView.Adapter<PendingUserListRecycleViewAdapter.ViewHolder> {

    private ArrayList<ParseUserModel> mListOfPendingPassengers;
    private PendingUserListRecycleViewItemClickListener mClickListener;


    public PendingUserListRecycleViewAdapter(ArrayList<ParseUserModel> listOfPendingPassengers, PendingUserListRecycleViewItemClickListener clickListener) {
        mListOfPendingPassengers = listOfPendingPassengers;
        mClickListener = clickListener;
    }

    public interface PendingUserListRecycleViewItemClickListener {
        void onItemClick(View v, int position);
    }

    @Override
    public PendingUserListRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_user_list, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final PendingUserListRecycleViewAdapter.ViewHolder holder, final int position) {

        final ParseUserModel userModel = mListOfPendingPassengers.get(position);
        Log.d(RoadPalApplication.TAG, "position: " + position);
        holder.textViewUser.setText(userModel.get_full_name());
        holder.textViewEmail.setText(userModel.get_email());

        ParseFile photoImage = userModel.get_users_image();

        if (photoImage != null) {
            photoImage.getFileInBackground(new GetFileCallback() {
                @Override
                public void done(File file, ParseException e) {
                    if (e == null) {
                        Log.d(RoadPalApplication.TAG, "file.getAbsolutePath(): " + file.getAbsolutePath());
                        Drawable usersDrawable= Drawable.createFromPath(file.getAbsolutePath());
                        holder.ImageViewUsersImage.setImageDrawable(usersDrawable);
                    } else {
                        Log.d(RoadPalApplication.TAG, "PendingUserListRecycleViewAdapter error on retrieving users image: " + e.getLocalizedMessage());
                    }
                }
            });
        } else {
            holder.ImageViewUsersImage.setImageDrawable(RoadPalApplication.getContext().getResources().getDrawable(R.drawable.ic_person));
        }

        // it is not possible to make this class implement View.onClickListener because of the position
        holder.buttonAcceptPassenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(v, position);
            }
        });
        holder.buttonDeclinePassenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(v, position);
            }
        });
        holder.clickableRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(v, position);
            }
        });


    }


    @Override
    public int getItemCount() {
        if (mListOfPendingPassengers != null) {
            return mListOfPendingPassengers.size();
        } else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        @Bind(R.id.button_accept_passenger)
        Button buttonAcceptPassenger;
        @Bind(R.id.button_decline_passenger)
        Button buttonDeclinePassenger;
        @Bind(R.id.image_view_users_image)
        ImageView ImageViewUsersImage;
        @Bind(R.id.clickable_relative_layout)
        RelativeLayout clickableRelativeLayout;
        @Bind(R.id.textView_user)
        TextView textViewUser;
        @Bind(R.id.text_view_email)
        TextView textViewEmail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }
}
