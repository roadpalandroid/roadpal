package com.rootcave.roadpal.ride.customViews;

import android.view.View;
import android.view.ViewGroup;

import com.rootcave.roadpal.R;

/**
 * Created by Lenovo T420 on 8.12.2015..
 */
public class ViewGroupExampleAdapter extends FancyCoverFlowAdapter {
    // =============================================================================
    // Private members
    // =============================================================================

    private int[] images = {R.drawable.android_logo, R.drawable.android_logo, R.drawable.android_logo};

    // =============================================================================
    // Supertype overrides
    // =============================================================================

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Integer getItem(int i) {
        return images[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getCoverFlowItem(int i, View reuseableView, ViewGroup viewGroup) {
        CustomViewGroup customViewGroup = null;

        if (reuseableView != null) {
            customViewGroup = (CustomViewGroup) reuseableView;
        } else {
            customViewGroup = new CustomViewGroup(viewGroup.getContext());
            customViewGroup.setLayoutParams(new FancyCoverFlow.LayoutParams(100, 200));
        }

        customViewGroup.getImageView().setImageResource(this.getItem(i));
        switch (i) {
            case 0:
                customViewGroup.getTextView().setText("Both");
                break;
            case 1:
                customViewGroup.getTextView().setText("Women Only");
                break;

            case 2:
                customViewGroup.getTextView().setText("Men Only");
                break;
        }
        return customViewGroup;
    }

}
