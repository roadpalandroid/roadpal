package com.rootcave.roadpal.ride.rideDetail;

import com.rootcave.roadpal.ride.RideModel;
import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 28.12.2015..
 */
public class RideDetailPresenterImpl implements IRideDetailPresenter, IonProcessJoinRideFinishedListener {

    private IRideDetailsView mRideDetailsView;
    private IRideDetailInteractor mRideDetailInteractor;

    public RideDetailPresenterImpl(IRideDetailsView view) {
        mRideDetailsView = view;
        mRideDetailInteractor = new RideDetailInteractorImpl(this);
    }

    @Override
    public void processJoinRide(RideModel rideModel) {
        mRideDetailInteractor.sendJoinRideRequest( rideModel);
        mRideDetailsView.showProgress();
    }


    @Override
    public void onProcessJoinRideSucess() {

        mRideDetailsView.hideProgress();
    }

    @Override
    public void onProcessJoinRideFailure(ParseException e) {
        mRideDetailsView.hideProgress();
    }


}
