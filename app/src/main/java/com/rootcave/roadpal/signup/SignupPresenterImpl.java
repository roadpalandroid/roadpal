package com.rootcave.roadpal.signup;

import android.graphics.Bitmap;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public class SignupPresenterImpl implements ISignupPresenter, IonSignupFinishedListener {

    private ISignupView signupView;
    private ISignupInteractor signupInteractor;

    public SignupPresenterImpl(ISignupView signupView) {
        this.signupView = signupView;
        signupInteractor = new SignupInteractorImpl(this);
    }

    @Override
    public void processInputData(String fullName, String username, String password, String email,String dob,String gender,String phone,String vehicleType,String vehiclePlates,Bitmap bitmap) {
        signupInteractor.validateInputData(fullName, username, password, email, dob,gender, phone,vehicleType,vehiclePlates, bitmap);
        signupView.showProgress();
    }

    @Override
    public void OnIncompleteCredentials() {
        signupView.onIncompleteCredentials();
        signupView.hideProgress();
    }

    @Override
    public void onSignupSucess() {
        signupView.onSignupSucess();
        signupView.hideProgress();
        signupView.navigateToNewsFeedActivity();
    }
    @Override
    public void onSignupFail(ParseException e) {
        if (e.getCode() == ParseException.USERNAME_TAKEN) {
            signupView.onUsernameTaken();
        } else if (e.getCode() == ParseException.INVALID_EMAIL_ADDRESS) {
            signupView.onInvalidEmail();
        } else if (e.getCode() == ParseException.EMAIL_TAKEN) {
            signupView.onEmailTaken();
        } else {
            signupView.onOtherError(e);
        }
        signupView.hideProgress();
    }
}
