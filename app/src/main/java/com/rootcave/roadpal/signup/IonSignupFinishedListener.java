package com.rootcave.roadpal.signup;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface IonSignupFinishedListener {
    void OnIncompleteCredentials();
    void onSignupSucess();
    void onSignupFail(ParseException e);

}
