package com.rootcave.roadpal.signup;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface ISignupView {
    void onIncompleteCredentials();
    void onSignupSucess();
    void onUsernameTaken();
    void  onInvalidEmail();
    void onEmailTaken();
    void onOtherError(ParseException e);
    void showProgress();
    void hideProgress();
    void navigateToNewsFeedActivity();

}
