package com.rootcave.roadpal.signup;

import android.graphics.Bitmap;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface ISignupInteractor {
    void validateInputData(String fullname, String username, String password, String email,String dob,String gender,String phone ,String vehicleType,String vehiclePlates,Bitmap bitmap);
    void signUpWithParse();
}
