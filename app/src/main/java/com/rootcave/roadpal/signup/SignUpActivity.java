package com.rootcave.roadpal.signup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.andexert.library.RippleView;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BitmapUtils;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.mainFeed.MainFeedActivity;
import com.parse.ParseException;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;


import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Activity which displays a login screen to the user.
 */
public class SignUpActivity extends AppCompatActivity implements ISignupView, View.OnClickListener, DatePickerDialog.OnDateSetListener {


    private static final String[] ITEMS = {"male", "female"};
    private ArrayAdapter<String> genderSpinnerAdapter;
    private static int IMAGE_PICK_REQUEST = 101;

    private boolean shown = false;
    // UI references.

    @Bind(R.id.fullname_edit_text) MaterialEditText fullnameEditText;
    @Bind(R.id.username_edit_text) MaterialEditText usernameEditText;
    @Bind(R.id.email_edit_text) MaterialEditText emailEditText;
    @Bind(R.id.password_edit_text) MaterialEditText passwordEditText;
    @Bind(R.id.password_again_edit_text) MaterialEditText passwordAgainEditText;
    @Bind(R.id.dob_edit_text) MaterialEditText dobEditText;
    @Bind(R.id.mobilePhone_edit_text) MaterialEditText mobilePhoneEditText;
    @Bind(R.id.bsignup_button) Button mSignupButton;
    @Bind(R.id.tvAddPersonImage) TextView tvAddPersonImage;
    @Bind(R.id.ivPersonImage) ImageView ivPersonImage;
    @Bind(R.id.genderSpinner) MaterialSpinner genderSpinner;
    @Bind(R.id.i_have_a_car_checkbox)CheckBox iHaveACarCheckbox;

    @Bind(R.id.vehicle_type_edit_text)MaterialEditText vehicleTypeEditText;
    @Bind(R.id.vehicle_plates_edit_text)MaterialEditText vehiclePlateNumberEditText;

    @Bind(R.id.vehicle_type_ripple_view)RippleView  vehicleTypeRippleView;
    @Bind(R.id.vehicle_plates_ripple_view)RippleView  vehiclePlatesRippleView;

    private String username;
    private String password;
    private String passwordAgain;
    private String fullname;
    private String email;
    private String dob;
    private String gender;
    private String mobilePhone;
    private String vehicleType;
    private String vehiclePlates;

    private Bitmap userImageBitmap;
    private ISignupPresenter signupPresenter;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);


        passwordAgainEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                password = passwordEditText.getText().toString().trim();
                passwordAgain = passwordAgainEditText.getText().toString().trim();

                // if (password.length() == passwordAgain.length()) {
                if (password.equals(passwordAgain)) {
                    //TODO either show error message below password again(text view) or implement it inside edittext controls(passwordEditText and passwordAgainEditText)
                    Log.d(RoadPalApplication.TAG, "Signupactivity passwords are equal");
                } else {
                    passwordAgainEditText.setError("Passwords do not match!");
                    Log.d(RoadPalApplication.TAG, "Signupactivity passwords are NOT equal");
                }

                // }
            }
        });

        // Set up the submit button click handler
        signupPresenter = new SignupPresenterImpl(this);

        mSignupButton.setOnClickListener(this);
        dobEditText.setOnClickListener(this);
        tvAddPersonImage.setOnClickListener(this);
        iHaveACarCheckbox.setOnClickListener(this);

        genderSpinnerAdapter = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_spinner_dropdown_item, ITEMS);
        //spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderSpinnerAdapter);
        genderSpinner.setPaddingSafe(0, 0, 0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bsignup_button:

                // Retrieve the text entered from the EditText
                Log.d(RoadPalApplication.TAG, "signup button listener: " + v.getId());
                fullname = fullnameEditText.getText().toString().trim();
                username = usernameEditText.getText().toString().trim();

                email = emailEditText.getText().toString().trim();
                dob = dobEditText.getText().toString().trim();
                gender = genderSpinner.getSelectedItem().toString();
                mobilePhone = mobilePhoneEditText.getText().toString().trim();
                vehicleType=vehicleTypeEditText.getText().toString().trim();
                vehiclePlates=vehiclePlateNumberEditText.getText().toString().trim();
              /*  ParseUser newUser= new ParseUser();
                newUser.setUsername(username);
                newUser.setPassword(password);
                newUser.setEmail(email);
                newUser.put("fullname", fullname);
                newUser.put("dob", dob);
                newUser.put("gender",gender);
                newUser.put("phone",mobilePhone);
                newuser.
                */
                signupPresenter.processInputData(fullname, username, password, email, dob, gender, mobilePhone,vehicleType,vehiclePlates, userImageBitmap);
                break;

            case R.id.dob_edit_text:
                Log.d(RoadPalApplication.TAG, "date picker edit text listener: " + v.getId());
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        SignUpActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.showYearPickerFirst(true);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;

            case R.id.tvAddPersonImage:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, IMAGE_PICK_REQUEST);
                break;

            case R.id.i_have_a_car_checkbox:
                if(((CheckBox)v).isChecked())
                {
                    vehicleTypeRippleView.setVisibility(View.VISIBLE);
                    vehiclePlatesRippleView.setVisibility(View.VISIBLE);
                 //  vehicleTypeEditText.setEnabled(true);
                 //   vehiclePlateNumberEditText.setEnabled(true);
                  //  vehicleTypeEditText.setVisibility(View.VISIBLE);
                  //  vehiclePlateNumberEditText.setVisibility(View.VISIBLE);


                }
                else
                {

                    vehicleTypeRippleView.setVisibility(View.GONE);
                    vehiclePlatesRippleView.setVisibility(View.GONE);
                  /*
                    vehicleTypeEditText.setEnabled(false);
                    vehiclePlateNumberEditText.setEnabled(false);
                    vehicleTypeEditText.setVisibility(View.INVISIBLE);
                    vehiclePlateNumberEditText.setVisibility(View.INVISIBLE);
*/
                }
                break;
        }
    }

    //image picker result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICK_REQUEST) {
                String filePathMame = BitmapUtils.getRealPathFromURI(imageReturnedIntent.getData(), this);
                BitmapFactory.Options options = new BitmapFactory.Options();
                userImageBitmap = BitmapFactory.decodeFile(filePathMame, options);
                userImageBitmap = Bitmap.createScaledBitmap(userImageBitmap, 200, 200
                        * userImageBitmap.getHeight() / userImageBitmap.getWidth(), false);
                ivPersonImage.setImageBitmap(userImageBitmap);
            }
        }
    }

    @Override
    public void onIncompleteCredentials() {
        Log.d("LoginSignupActivity", "Please complete the registration form");
        Toast.makeText(SignUpActivity.this, "Please complete registration form", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSignupSucess() {

            Log.d("SignupActivity", "Successfully registered.");
            Toast.makeText(SignUpActivity.this, "Successfully registered.", Toast.LENGTH_LONG).show();
}

    @Override
    public void onUsernameTaken() {
        Log.d("LoginSignupActivity", "username already taken, please try again.");
        Toast.makeText(SignUpActivity.this,
                "username already taken, please try again",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInvalidEmail() {
        Log.d("LoginSignupActivity", "invalid email, please try again.");
        Toast.makeText(SignUpActivity.this,
                "invalid email, please try again",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onEmailTaken() {
        Log.d("LoginSignupActivity", "the email address  has already been taken");
        Toast.makeText(SignUpActivity.this,
                "the email address " + email + " has already been taken",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOtherError(ParseException e) {
        Log.d("LoginSignupActivity", "Sign up Error");
        Toast.makeText(SignUpActivity.this,
                e.getMessage().toString(), Toast.LENGTH_LONG)
                .show();
        e.printStackTrace();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_signup));
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void navigateToNewsFeedActivity() {

        startActivity(new Intent(SignUpActivity.this, MainFeedActivity.class));
        finish();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        dobEditText.setText(date);

    }
}
