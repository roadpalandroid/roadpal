package com.rootcave.roadpal.signup;


import android.graphics.Bitmap;
import android.util.Log;

import com.rootcave.roadpal.common.BitmapUtils;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.common.SaveToPrefsUtil;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public class SignupInteractorImpl implements ISignupInteractor, SignUpCallback {

    private String fullName;
    private String username;
    private String password;
    private String email;
    private String dob;
    private String gender;
    private String phone;
    private String vehicleType;
    private String vehiclePlates;
    private boolean pushEnabled;
    private Bitmap userImageBitmap;
    private IonSignupFinishedListener mListener;
    private ParseUserModel user;

    public SignupInteractorImpl(IonSignupFinishedListener listener) {
        mListener = listener;
    }

    @Override
    public void validateInputData(String fullName, String username, String password, String email,String dob,String gender,String phone,String vehicleType,String vehiclePlates,Bitmap bitmap) {
        this.fullName = fullName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.dob=dob;
        this.gender=gender;
        this.phone=phone;
        this.vehicleType=vehicleType;
        this.vehiclePlates=vehiclePlates;
        this.userImageBitmap=bitmap;
        pushEnabled=true;

        // Force user to fill up the form
        if (username.equals("") || password.equals("") || email.equals("")) {
            mListener.OnIncompleteCredentials();

        } else {
            signUpWithParse();
        }
    }

    @Override
    public void signUpWithParse() {
        // Save new user data into Parse.com Data Storage
        ParseUser newUser=new ParseUser();
        user= new ParseUserModel(newUser);


        user.set_username(username);
        user.set_password(password);
        user.set_email(email);

        user.set_fullname(fullName);
        user.set_dob(dob);
        user.set_gender(gender);
        user.set_phone(phone);
        user.set_vehicle_model(vehicleType);
        user.set_vehicle_plates(vehiclePlates);
        user.set_push_enabled(pushEnabled);

        if (userImageBitmap != null) {
            // userImageBitmap = BitmapFactory.decodeResource(RoadPalApplication.getContext().getResources(), R.drawable.profile);
            byte[] user_image = BitmapUtils.bitmapToByteArray(userImageBitmap);
            final ParseFile photoImage = new ParseFile(user_image);

            photoImage.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d(RoadPalApplication.TAG, "Image saved succesfully");
                        user.set_users_image(photoImage);

                        user.sign_up_in_background(SignupInteractorImpl.this);
                    }
                }
            });
        }
        else{
            // signup without adding profile picture
            user.sign_up_in_background(SignupInteractorImpl.this);
        }
    }

    @Override
    public void done(ParseException e) {
        if (e == null) {
            //TODO needed for push, either implement it here and in login interactor or  only in main feed activity
            RoadPalApplication.updateParseInstallation(ParseUser.getCurrentUser());


            SaveToPrefsUtil.saveToSharedPrefs(user);


            mListener.onSignupSucess();
            // Show a simple Toast message upon successful registration
        } else {
            mListener.onSignupFail(e);
        }
    }

}
