package com.rootcave.roadpal.Login;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Lenovo T420 on 18.11.2015..
 */
public interface ILoginInteractor {
    void login(String username, String password);
    void logInWithParse(String username, String password);
    void logInWithFacebook(AppCompatActivity activity);

}
