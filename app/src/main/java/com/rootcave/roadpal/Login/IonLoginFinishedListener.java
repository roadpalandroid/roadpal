package com.rootcave.roadpal.Login;


import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 18.11.2015..
 */
public interface IonLoginFinishedListener  {
    void onUsernameError();
    void onPasswordError();
    void onLoginSucess();
    void onParseloginError(ParseException e);
}
