package com.rootcave.roadpal.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.mainFeed.MainFeedActivity;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.rengwuxian.materialedittext.MaterialEditText;


import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Activity which displays a login screen to the user, offering registration as well.
 */
public class LoginActivity extends AppCompatActivity implements IloginView, View.OnClickListener, TextView.OnEditorActionListener {
    // UI references.


    @Bind(R.id.username) MaterialEditText usernameEditText;
    @Bind(R.id.password) MaterialEditText passwordEditText;
    @Bind(R.id.facebook_login_button) Button facebookLoginButton;
    @Bind(R.id.action_button)Button actionButton;

    private ProgressDialog progressDialog;
    private ILoginPresenter loginPresenter;
    private StringBuilder validationErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        // Set up the login form.
        passwordEditText.setOnEditorActionListener(this);
        // Set up the submit button click handler
        actionButton.setOnClickListener(this);
        loginPresenter = new LoginPresenterImpl(this);
    }

    public void onFacebookLoginClick(View v) {
        loginPresenter.loginWithFacebook(LoginActivity.this);
    }

    //result from facebook activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        Log.d(RoadPalApplication.TAG, "on facebookActivityResult!__________________________");
       // navigateToHome();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(getString(R.string.progress_login));

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void setUsernameError() {
        validationErrorMessage = new StringBuilder(getString(R.string.error_intro));
        validationErrorMessage.append(getString(R.string.error_blank_username));
        validationErrorMessage.append(getString(R.string.error_end));
        usernameEditText.setError(validationErrorMessage.toString());
        Toast.makeText(LoginActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void setPasswordError() {
        validationErrorMessage = new StringBuilder(getString(R.string.error_intro));
        validationErrorMessage.append(getString(R.string.error_blank_password));
        validationErrorMessage.append(getString(R.string.error_end));
        passwordEditText.setError(validationErrorMessage.toString());
        Toast.makeText(LoginActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void setParseLoginError(ParseException e) {
        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(this, MainFeedActivity.class);
        startActivity(intent);
        Log.d("LoginActivity", "Successfully Logged in");
        Toast.makeText(getApplicationContext(),
                "Successfully Logged in",
                Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onClick(View v) {
        loginPresenter.validateCredentials(usernameEditText.getText().toString().trim(), passwordEditText.getText().toString().trim());
        // login();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == R.id.edittext_action_login ||
                actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
          //  loginPresenter.validateCredentials(usernameEditText.getText().toString().trim(), passwordEditText.getText().toString().trim());

            //TODO leaked window show progress, probably because showprogress gets called twice
            return true;
        }
        return false;

    }


}
