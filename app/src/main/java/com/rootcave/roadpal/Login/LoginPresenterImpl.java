package com.rootcave.roadpal.Login;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Lenovo T420 on 18.11.2015..
 */
public class LoginPresenterImpl implements ILoginPresenter, IonLoginFinishedListener {
    private IloginView loginView;
    private ILoginInteractor loginInteractor;

    public LoginPresenterImpl(IloginView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl(this);
    }

    //methods from ILoginPresenter
    @Override
    public void validateCredentials(String username, String password) {

        loginView.showProgress();
        loginInteractor.login(username, password);
    }

    @Override
    public void loginWithFacebook(AppCompatActivity activity) {
        loginInteractor.logInWithFacebook(activity);
        loginView.showProgress();

    }
    //methods from IonLoginFinishedListener
    @Override
    public void onUsernameError() {
        loginView.hideProgress();
        loginView.setUsernameError();

    }

    @Override
    public void onPasswordError() {
        loginView.hideProgress();
        loginView.setPasswordError();

    }

    @Override
    public void onLoginSucess() {
        loginView.hideProgress();
        loginView.navigateToHome();

    }

    @Override
    public void onParseloginError(com.parse.ParseException e) {
        loginView.hideProgress();
        loginView.setParseLoginError(e);
    }


}
