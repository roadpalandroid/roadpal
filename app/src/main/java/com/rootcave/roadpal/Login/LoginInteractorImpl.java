package com.rootcave.roadpal.Login;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.parse.Parse;
import com.rootcave.roadpal.common.BitmapUtils;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.common.SaveToPrefsUtil;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Lenovo T420 on 18.11.2015..
 */
public class LoginInteractorImpl implements ILoginInteractor, LogInCallback, GraphRequest.GraphJSONObjectCallback, SaveCallback {
    private final List<String> permissions = Arrays.asList("public_profile", "email");
    // private ParseUser currentUser;
    private ParseUserModel currentUser;
    private Profile mFbProfile;


    private IonLoginFinishedListener mListener;

    public LoginInteractorImpl(IonLoginFinishedListener mListener) {
        this.mListener = mListener;
    }


    @Override
    public void login(final String username, final String password) {

        boolean validationError = false;
        if (username.length() == 0) {
            validationError = true;
            mListener.onUsernameError();
        }
        if (password.length() == 0) {
            if (validationError) {
            }
            validationError = true;
            mListener.onPasswordError();
        }
        if (!validationError) {
            logInWithParse(username, password);
        }
    }

    @Override
    public void logInWithParse(String username, String password) {
        ParseUser.logInInBackground(username, password, this);
    }

    @Override
    public void done(ParseUser parseUser, ParseException e) {
        if (e != null) {
            // Show the error message
            //TODO make error dialog instead of toast message
            Toast.makeText(RoadPalApplication.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            mListener.onParseloginError(e);
        } else {
            //TODO needed for push, either implement it here and in signup interactor or  only in main feed activity
            RoadPalApplication.updateParseInstallation(parseUser);
            mListener.onLoginSucess();
            currentUser = new ParseUserModel(parseUser);
            SaveToPrefsUtil.saveToSharedPrefs(currentUser);

            Log.d(RoadPalApplication.TAG, "get phone: " + parseUser.get("phone"));

        }
    }

    //TODO refactor whole class its very unclean
    @Override
    public void logInWithFacebook(AppCompatActivity activity) {
        ParseFacebookUtils.logInWithReadPermissionsInBackground(activity, permissions, new LogInCallback() {
            @Override
            public void done(final ParseUser parseUser, ParseException e) {
                if (parseUser == null) {
                    Log.d(RoadPalApplication.TAG, "Uh oh. The user cancelled the Facebook login.");
                } else if (parseUser.isNew()) {
                    Log.d(RoadPalApplication.TAG, "User signed up and logged in through Facebook!");
                    currentUser = new ParseUserModel(parseUser);
                    mFbProfile = Profile.getCurrentProfile();
                    //TODO needed for push, either implement it here and in signup interactor or  only in main feed activity
                    RoadPalApplication.updateParseInstallation(parseUser);
                    getUserDetailsFromFacebook();

                } else {
                    Log.d(RoadPalApplication.TAG, "User logged in through Facebook!");
                    //TODO  needed for push, either implement it here and in signup interactor or  only in main feed activity
                    if(e!=null)
                    {
                        Log.d(RoadPalApplication.TAG, "parse exception:!" + e.getLocalizedMessage());
                    }

                    Log.d(RoadPalApplication.TAG, "object id:!" +parseUser.getObjectId());
                    Log.d(RoadPalApplication.TAG, "username:!" + parseUser.getUsername());
                    Log.d(RoadPalApplication.TAG, "email:" + parseUser.getEmail());
                    Log.d(RoadPalApplication.TAG, "current user object id:!" + ParseUser.getCurrentUser().getObjectId());
                    RoadPalApplication.updateParseInstallation(ParseUser.getCurrentUser());
                  //remove save to prefs from here when test users are removed
                    currentUser = new ParseUserModel(parseUser);
                    SaveToPrefsUtil.saveToSharedPrefs(currentUser);
                    //  getUserDetailsFromFacebook();
                    mListener.onLoginSucess();
                }
            }
        });
    }

    private void getUserDetailsFromFacebook() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), this);
        Bundle parameters = new Bundle();
        //   parameters.putString("fields", "id,email,gender,name,picture");   // fetches small picture
        //  parameters.putString("fields", "id,email,gender,name,birthday,picture.type(large)");
        parameters.putString("fields", "id,email,gender,name,birthday,picture.type(small)");
        request.setParameters(parameters);

        request.executeAsync();
    }

    /*
        Method extracts user information from facebook account
        */
    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
        //TODO  json object return null , check out what is wrong
        if (jsonObject != null) {
            try {
                // currentUser = (ParseUserModel)ParseUser.getCurrentUser();
                currentUser.set_username(jsonObject.getString("name"));
                if (jsonObject.getString(RoadPalApplication.KEY_GENDER) != null)
                    currentUser.set_gender(jsonObject.getString("gender"));
                if (jsonObject.getString(RoadPalApplication.KEY_EMAIL) != null)
                    currentUser.set_email(jsonObject.getString("email"));
            /*    if (jsonObject.getString("birthday") != null)
                    currentUser.put("dob",jsonObject.getString("birthday"));
                    */
                FbProfilePhotoAsync profilePhotoAsync = new FbProfilePhotoAsync(mFbProfile);
                profilePhotoAsync.execute();

            } catch (JSONException e) {
                Log.d(RoadPalApplication.TAG,
                        "Error parsing returned user data. " + e);
            }
        } else if (graphResponse.getError() != null) {
            Log.d(RoadPalApplication.TAG, "graph response: " + graphResponse.getError().toString() + "______________________________");
            switch (graphResponse.getError().getCategory()) {
                case LOGIN_RECOVERABLE:
                    Log.d(RoadPalApplication.TAG,
                            "Authentication error: " + graphResponse.getError());
                    break;

                case TRANSIENT:
                    Log.d(RoadPalApplication.TAG,
                            "Transient error. Try again. " + graphResponse.getError());
                    break;

                case OTHER:
                    Log.d(RoadPalApplication.TAG,
                            "Some other error: " + graphResponse.getError());
                    break;
            }
        }
    }

    class FbProfilePhotoAsync extends AsyncTask<String, String, String> {
        Profile profile;
        public Bitmap bitmap;

        public FbProfilePhotoAsync(Profile profile) {
            this.profile = profile;
        }

        @Override
        protected String doInBackground(String... params) {
            // Fetching data from URI and storing in bitmap
            Log.d(RoadPalApplication.TAG, "doinbackground profile pic: " + profile.getProfilePictureUri(140, 140).toString());
            bitmap = BitmapUtils.downloadImageBitmap(profile.getProfilePictureUri(140, 140).toString());

            byte[] data = BitmapUtils.bitmapToByteArray(bitmap);
            final ParseFile parseFile = new ParseFile(data);

            try {
                parseFile.save();
                Log.d(RoadPalApplication.TAG, "Saving of facebook user image to parse sucess");
                currentUser.set_push_enabled(true);
                currentUser.set_users_image(parseFile);
                //Finally save all the user details
                // currentUser.save_user();
                currentUser.save_user_in_background(LoginInteractorImpl.this);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(RoadPalApplication.TAG, "Saving of facebook users image to parse error: " + e.getLocalizedMessage());
            }

            return params.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(RoadPalApplication.TAG, "on post execute:  " + s);
            super.onPostExecute(s);

        }
    }

    @Override
    public void done(ParseException e) {
        //TODO implement error cases
        if (e == null) {
            Toast.makeText(RoadPalApplication.getContext(), "New user:" + mFbProfile.getFirstName() + " Signed up", Toast.LENGTH_SHORT).show();
            Log.d(RoadPalApplication.TAG, "New user:" + mFbProfile.getFirstName() + " Signed up");
            SaveToPrefsUtil.saveToSharedPrefs(currentUser);
            mListener.onLoginSucess();
        } else {
            Log.d(RoadPalApplication.TAG, "Final saving  of user  to parse error: " + e.getLocalizedMessage());

        }
    }

}
