package com.rootcave.roadpal.Login;

/**
 * Created by Lenovo T420 on 18.11.2015..
 */
public interface IloginView {
     void showProgress();

     void hideProgress();

     void setUsernameError();

     void setPasswordError();

     void setParseLoginError(com.parse.ParseException e);

     void navigateToHome();
}
