package com.rootcave.roadpal.message.chat;

import android.os.Handler;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.message.Conversation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Lenovo T420 on 16.2.2016..
 */
public class ChatInteractorImpl implements IChatInteractor, Runnable {

    private IOnChatProcessedFinishedListener mListener;
    private ParseUserModel chatUser;
    private ParseUser currentUser;
    private ArrayList<Conversation> mConvList;
    private boolean isOnline;
    private Handler handler = new Handler();
    private Runnable runnable = this;
    /**
     * The date of last message in conversation.
     */
    private Date lastMsgDate = null;


    public ChatInteractorImpl(IOnChatProcessedFinishedListener listener) {
        mListener = listener;
    }

    //TODO refactor both methods, implement refreshing of chat for new messages
    @Override
    public void loadConversationList(final ArrayList<Conversation> convList) {
        mConvList = convList;


        currentUser = ParseUser.getCurrentUser();
        chatUser = EventBus.getDefault().getStickyEvent(ParseUserModel.class);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat");
        if (convList.size() == 0) {
            // load all messages...
            ArrayList<String> al = new ArrayList<String>();
            al.add(chatUser.get_username());
            al.add(currentUser.getUsername());
            query.whereContainedIn("sender", al);
            query.whereContainedIn("receiver", al);

        } else {
            // load only newly received message..
            if (lastMsgDate != null)
                query.whereGreaterThan("createdAt", lastMsgDate);
            query.whereEqualTo("sender", chatUser.get_username());
            query.whereEqualTo("receiver", currentUser.getUsername());

        }
        query.orderByDescending("createdAt");
        query.setLimit(30);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> chatList, ParseException e) {

                if (e != null) {
                    mListener.onLoadConversationListFailure(e);
                    handler.postDelayed(runnable, 3000);

                } else {
                    if (chatList != null && chatList.size() > 0) {
                        for (int i = chatList.size() - 1; i >= 0; i--) {
                            ParseObject chatObject = chatList.get(i);
                            Conversation conversation = new Conversation(chatObject
                                    .getString("message"), chatObject.getCreatedAt(), chatObject
                                    .getString("sender"));
                            mConvList.add(conversation);

                            if (lastMsgDate == null || lastMsgDate.before(conversation.getDate()))
                                lastMsgDate = conversation.getDate();
                        }

                        mListener.onLoadConversationListSucess(chatUser);
                        handler.postDelayed(runnable, 3000);


                    }
                    if (chatList.size() == 0) {
                        mListener.onLoadConversationListEmpty(chatUser);
                        handler.postDelayed(runnable, 3000);
                        Log.d(RoadPalApplication.TAG, "conv list empty!");

                    }
                }
            }
        });


    }

    @Override
    public void sendMessage(String text) {
        // String messageText = text;
        final Conversation conversation = new Conversation(text, new Date(),
                currentUser.getUsername());
        conversation.setStatus(Conversation.STATUS_SENDING);
        mConvList.add(conversation);

        mListener.onSendMessageSucess();
        ParseObject po = new ParseObject("Chat");
        po.put("sender", currentUser.getUsername());
        po.put("receiver", chatUser.get_username());
        // po.put("createdAt", "");
        po.put("message", text);
        po.saveEventually(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    conversation.setStatus(Conversation.STATUS_SENT);
                    mListener.onSendMessageSucess();
                } else {
                    conversation.setStatus(Conversation.STATUS_FAILED);
                    mListener.onSendMessageFailure(e);
                }


            }
        });
    }

    @Override
    public void setActivityRunningStatus(boolean isOnline) {
        this.isOnline = isOnline;
    }

    @Override
    public void run() {
        Log.d(RoadPalApplication.TAG, "runnable runned!");
        if (isOnline) {
            loadConversationList(mConvList);
        } else {

            handler.removeCallbacks(runnable);
        }
    }
}
