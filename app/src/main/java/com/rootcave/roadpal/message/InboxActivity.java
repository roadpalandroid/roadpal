package com.rootcave.roadpal.message;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.common.ZoomOutPageTransformer;
import com.rootcave.roadpal.message.chat.ChatActivity;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.myRides.pendingUsers.PendingRequestsActivity;

import de.greenrobot.event.EventBus;

public class InboxActivity extends BaseActivity implements TabLayout.OnTabSelectedListener ,MessagesFragment.Callbacks {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        super.onCreateToolbar();

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Messages"));
        tabLayout.addTab(tabLayout.newTab().setText("Notifications"));
       // tabLayout.addTab(tabLayout.newTab().setText("Contact"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(this);




    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        Log.d(RoadPalApplication.TAG, "Tab " + tab.getPosition() + " selected!");
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    //from MessagesFragment.Callbacks
    @Override
    public void onItemSelected(ParseUserModel userModel) {
        Log.d(RoadPalApplication.TAG, "onitem selected InboxActivity");
        launchChatActivity(userModel);
    }

    private void launchChatActivity(ParseUserModel model) {
        EventBus.getDefault().postSticky(model);
        Intent userIntent= new Intent(this, ChatActivity.class);
        startActivity(userIntent);
        Log.d(RoadPalApplication.TAG, "  ChatActivity launched from my InboxActivity ! ");

    }
}
