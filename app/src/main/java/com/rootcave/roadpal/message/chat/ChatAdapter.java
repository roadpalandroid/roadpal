package com.rootcave.roadpal.message.chat;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.message.Conversation;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 16.2.2016..
 */

public class ChatAdapter extends BaseAdapter {


    private ArrayList<Conversation> mConvList;
    private final Context mContext;


    public ChatAdapter(Context context, ArrayList<Conversation> convList) {
        mContext = context;
        mConvList = convList;
    }


    /* (non-Javadoc)
         * @see android.widget.Adapter#getCount()
         */
    @Override
    public int getCount() {
        return mConvList.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Conversation getItem(int arg0) {
        return mConvList.get(arg0);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

		/* (non-Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */

    @Override
    public View getView(int pos, View v, ViewGroup arg2) {
        ViewHolder holder;
        Conversation c = getItem(pos);
        if (v == null) {

            if (c.isSent())
                v = LayoutInflater.from(mContext).inflate(R.layout.chat_item_sent, null);

            else
                v = LayoutInflater.from(mContext).inflate(R.layout.chat_item_rcv, null);

            holder = new ViewHolder();
            holder.lbl1 = (TextView) v.findViewById(R.id.lbl1);
            holder.lbl2 = (TextView) v.findViewById(R.id.lbl2);
            holder.lbl3 = (TextView) v.findViewById(R.id.lbl3);
            v.setTag(holder);

        } else {
            holder = (ViewHolder) v.getTag();
        }


        holder.lbl1.setText(DateUtils.getRelativeDateTimeString(mContext, c
                        .getDate().getTime(), DateUtils.SECOND_IN_MILLIS,
                DateUtils.DAY_IN_MILLIS, 0));

        holder.lbl2.setText(c.getMsg());


        if (c.isSent()) {
            if (c.getStatus() == Conversation.STATUS_SENT)
                holder.lbl3.setText("Delivered");
            else if (c.getStatus() == Conversation.STATUS_SENDING)
                holder.lbl3.setText("Sending...");
            else
                holder.lbl3.setText("Failed");
        } else
            holder.lbl3.setText("");

        return v;
    }

    static class ViewHolder {
        TextView lbl1;
        TextView lbl2;
        TextView lbl3;
    }


}