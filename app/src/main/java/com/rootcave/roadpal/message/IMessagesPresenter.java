package com.rootcave.roadpal.message;

import com.rootcave.roadpal.common.ParseUserModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 14.2.2016..
 */
public interface IMessagesPresenter {

    void updateUserStatus(boolean isOnline);

    void loadUserList(ArrayList<ParseUserModel> userList);



}
