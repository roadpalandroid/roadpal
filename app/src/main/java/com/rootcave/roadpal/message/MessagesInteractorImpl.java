package com.rootcave.roadpal.message;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 14.2.2016..
 */
public class MessagesInteractorImpl implements ImessagesInteractor, SaveCallback, FindCallback<ParseUser> {

    private IonLoadFinishedListener mListener;
    private ParseUserModel currentUser;
    private ArrayList<ParseUserModel> userList;

    public MessagesInteractorImpl(IonLoadFinishedListener listener) {
        mListener = listener;
        currentUser = new ParseUserModel(ParseUser.getCurrentUser());

    }

    @Override
    public void updateUserStatus(boolean isOnline) {

        currentUser.setOnlineStatus(isOnline);
        currentUser.save_user_eventually(this);
    }

    @Override
    public void loadUserList(ArrayList<ParseUserModel> userList) {
        this.userList = userList;
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereNotEqualTo("username", currentUser.get_username());
        query.findInBackground(this);
    }

    // SaveCallback
    @Override
    public void done(ParseException e) {
        Log.d(RoadPalApplication.TAG, "User status updated succesfully");
    }

    //FindCallback<ParseUser>
    @Override
    public void done(List<ParseUser> listOfParseUsers, ParseException e) {

        if (e != null) {
            mListener.onLoadUserListFailure(e);
        } else {
            if (listOfParseUsers.size() > 0) {
                for (ParseUser object : listOfParseUsers) {
                    ParseUserModel userModel = new ParseUserModel(object);
                    userList.add(userModel);
                }
                mListener.onLoadUserListSucess();
            }
            else
            {

                Log.d(RoadPalApplication.TAG, "0 users found ! ");
                mListener.onLoadUserListEmpty();
            }


        }
    }
}
