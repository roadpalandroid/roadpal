package com.rootcave.roadpal.message;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 14.2.2016..
 */
public interface IonLoadFinishedListener {


    void onLoadUserListSucess();

    void onLoadUserListEmpty();

    void onLoadUserListFailure(ParseException e);

}
