package com.rootcave.roadpal.message;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 14.2.2016..
 */
public class MessagesRecycleViewAdapter extends RecyclerView.Adapter<MessagesRecycleViewAdapter.ViewHolder> {

    private ArrayList<ParseUserModel> userList;

    public MessagesRecycleViewAdapter(ArrayList<ParseUserModel> userList) {
        this.userList = userList;

    }

    @Override
    public MessagesRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;


    }

    @Override
    public void onBindViewHolder(MessagesRecycleViewAdapter.ViewHolder holder, int position) {

        holder.chat_item_text_view.setText(userList.get(position).get_username());
        holder.chat_item_text_view.setCompoundDrawablesWithIntrinsicBounds(
                userList.get(position).isOnline() ? R.drawable.ic_online
                        : R.drawable.ic_offline, 0, R.drawable.arrow, 0);

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {
        @Bind(R.id.text_view_chat_item)
        TextView chat_item_text_view;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // itemView.setOnCreateContextMenuListener(this);
        }


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(RoadPalApplication.TAG, "Item on position: " + position + "clicked!");
        }
    }


}
