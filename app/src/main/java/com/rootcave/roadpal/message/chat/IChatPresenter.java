package com.rootcave.roadpal.message.chat;

import com.rootcave.roadpal.message.Conversation;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 16.2.2016..
 */
public interface IChatPresenter {

    void loadConversationList(ArrayList<Conversation> convList);
    void sendMessage(String text);

    void setActivityRunningStatus(boolean isOnline);
}
