package com.rootcave.roadpal.message.chat;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.parse.ParseException;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.BaseActivity;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.message.Conversation;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lenovo T420 on 15.2.2016..
 */
public class ChatActivity extends BaseActivity implements IChatView {

    static final String TAG = ChatActivity.class.getSimpleName();
    private ArrayList<Conversation> convList = new ArrayList<>();

    private ProgressDialog progressDialog;
    private ChatAdapter mAdapter;
    private IChatPresenter mPresenter;

    @Bind(R.id.list)
    ListView chatListView;
    @Bind(R.id.txt)
    EditText sendEditText;
    @Bind(R.id.btnSend)
    Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);
        Log.d(RoadPalApplication.TAG, "ChatActivity onCreate: ");
        super.onCreateToolbar();
        ButterKnife.bind(this);

        mAdapter = new ChatAdapter(this, convList);
        chatListView.setAdapter(mAdapter);
        chatListView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chatListView.setStackFromBottom(true);



        sendEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(RoadPalApplication.TAG, "Send button  clicked");
                mPresenter.sendMessage(sendEditText.getText().toString());
            }
        });

        //  getSupportActionBar().setTitle("User");

        mPresenter = new ChatPresenterImpl(this);
        mPresenter.loadConversationList(convList);
    }

    @Override
    protected void onResume() {
        mPresenter.setActivityRunningStatus(true);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mPresenter.setActivityRunningStatus(false);
        super.onPause();
    }


    @Override
    public void onLoadConversationListSucess(ParseUserModel chatUser) {
        mAdapter.notifyDataSetChanged();
        if(!getSupportActionBar().getTitle().equals(chatUser.get_username())) {
            getSupportActionBar().setTitle(chatUser.get_username());
        }
    }

    @Override
    public void onLoadConversationListFailure(ParseException e) {
        Log.d(RoadPalApplication.TAG, "Exception while loading conv listhappened: " + e.getLocalizedMessage());
    }

    @Override
    public void onLoadConversationListEmpty(ParseUserModel chatUser) {
        Log.d(RoadPalApplication.TAG, "conversation list empty!");
        if(!getSupportActionBar().getTitle().equals(chatUser.get_username())) {
            getSupportActionBar().setTitle(chatUser.get_username());
        }
    }

    @Override
    public void onSendMessageSucess() {
        mAdapter.notifyDataSetChanged();
        sendEditText.setText(null);

    }

    @Override
    public void onSendMessageFailure(ParseException e) {
        Log.d(RoadPalApplication.TAG, "Exception when sending message happened: " + e.getLocalizedMessage());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("need some cool loading text");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


}
