package com.rootcave.roadpal.message.chat;

import com.parse.ParseException;
import com.rootcave.roadpal.common.ParseUserModel;

/**
 * Created by Lenovo T420 on 16.2.2016..
 */
public interface IChatView {

    void onLoadConversationListSucess(ParseUserModel model);

    void onLoadConversationListFailure(ParseException e);

    void onLoadConversationListEmpty(ParseUserModel chatUser);

    void onSendMessageSucess();
    void onSendMessageFailure(ParseException e);
    void showProgress();
    void hideProgress();


}
