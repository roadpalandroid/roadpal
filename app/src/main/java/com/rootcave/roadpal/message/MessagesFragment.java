package com.rootcave.roadpal.message;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.common.RoadPalApplication;
import com.rootcave.roadpal.ride.RecyclerItemClickListener;
import com.rootcave.roadpal.ride.RideModel;
import com.rootcave.roadpal.ride.RideModelRecycleViewAdapter;
import com.rootcave.roadpal.ride.myRides.pendingUsers.PendingRequestsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment implements IMessagesView, RecyclerItemClickListener.OnItemClickListener {
    static final String TAG = MessagesFragment.class.getSimpleName();
    private IMessagesPresenter messagesPresenter;
    private ProgressDialog progressDialog;

    @Bind(R.id.recycler_view_messages)
    RecyclerView messagesRecyclerView;

    /**
     * The ChatActivity list.
     */
    private ArrayList<ParseUserModel> userList = new ArrayList<>();

    /**
     * The user.
     */
    public static ParseUser user;

    private MessagesRecycleViewAdapter mAdapter;

    private Callbacks mCallbacks = sDummyCallbacks;



    public static MessagesFragment newInstance()
    {
        MessagesFragment fragment= new MessagesFragment();
        return fragment;
    }


    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(ParseUserModel userModel);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(ParseUserModel userModel) {
        }
    };


    public MessagesFragment() {

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }
        if (messagesPresenter == null) {
            messagesPresenter = new MessagesPresenterImpl(this);
        }
        messagesPresenter.updateUserStatus(true);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        messagesPresenter = new MessagesPresenterImpl(this);
        messagesPresenter.loadUserList(userList);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this, view);

        messagesRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        messagesRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MessagesRecycleViewAdapter(userList);
        messagesRecyclerView.setAdapter(mAdapter);
        messagesRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        messagesPresenter.updateUserStatus(false);
        Log.d(RoadPalApplication.TAG, "inside on detach!");
        mCallbacks = sDummyCallbacks;
    }
    //RecyclerItemClickListener.OnItemClickListener
    @Override
    public void onItemClick(View view, int position) {
        mCallbacks.onItemSelected(userList.get(position));
        Log.d(RoadPalApplication.TAG, "position " + position + " clicked");
    }

    @Override
    public void onLoadUserListSucess() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadUserListFailure(ParseException e) {
        Log.d(RoadPalApplication.TAG, "Exception happened: " + e.getLocalizedMessage());
    }

    @Override
    public void onLoadUserListEmpty() {

        Log.d(RoadPalApplication.TAG, "UserList is empty");

    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("need some cool loading text");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


}