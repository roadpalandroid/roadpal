package com.rootcave.roadpal.message;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 14.2.2016..
 */
public interface IMessagesView {


    void onLoadUserListSucess();

    void onLoadUserListFailure(ParseException e);

    void showProgress();

    void hideProgress();

    void onLoadUserListEmpty();
}
