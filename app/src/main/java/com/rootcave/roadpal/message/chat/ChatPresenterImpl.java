package com.rootcave.roadpal.message.chat;

import com.parse.ParseException;
import com.rootcave.roadpal.common.ParseUserModel;
import com.rootcave.roadpal.message.Conversation;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 16.2.2016..
 */
public class ChatPresenterImpl implements IChatPresenter, IOnChatProcessedFinishedListener {

    private IChatView mChatView;
    private IChatInteractor mInteractor;

    public ChatPresenterImpl(IChatView chatView) {
        mChatView = chatView;
        mInteractor = new ChatInteractorImpl(this);

    }

    //from IChatPresenter
    @Override
    public void loadConversationList(ArrayList<Conversation> convList) {
        mInteractor.loadConversationList(convList);
        mChatView.showProgress();
    }

    @Override
    public void sendMessage(String text) {
        mInteractor.sendMessage(text);
        mChatView.showProgress();
    }

    @Override
    public void setActivityRunningStatus(boolean isOnline) {
        mInteractor.setActivityRunningStatus(isOnline);
    }


    //from IOnChatProcessedFinishedListener
    @Override
    public void onLoadConversationListSucess(ParseUserModel chatUser) {
        mChatView.hideProgress();
        mChatView.onLoadConversationListSucess(chatUser);
    }

    @Override
    public void onLoadConversationListFailure(ParseException e) {
        mChatView.hideProgress();
        mChatView.onLoadConversationListFailure(e);
    }

    @Override
    public void onLoadConversationListEmpty(ParseUserModel chatUser) {
        mChatView.hideProgress();
        mChatView.onLoadConversationListEmpty(chatUser);
    }

    @Override
    public void onSendMessageSucess() {
        mChatView.hideProgress();
        mChatView.onSendMessageSucess();
    }

    @Override
    public void onSendMessageFailure(ParseException e) {
        mChatView.hideProgress();
        mChatView.onSendMessageFailure(e);
    }


}
