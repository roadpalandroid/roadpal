package com.rootcave.roadpal.message;

import com.parse.ParseException;
import com.rootcave.roadpal.common.ParseUserModel;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 14.2.2016..
 */
public class MessagesPresenterImpl implements IMessagesPresenter, IonLoadFinishedListener {

    private ImessagesInteractor mMessagesInteractor;
    private IMessagesView mMessagesView;


    public MessagesPresenterImpl(IMessagesView messagesView) {
        mMessagesView = messagesView;
        mMessagesInteractor = new MessagesInteractorImpl(this);
    }



    @Override
    public void updateUserStatus(boolean isOnline) {
        mMessagesInteractor.updateUserStatus(isOnline);
    }

    @Override
    public void loadUserList(ArrayList<ParseUserModel> userList) {
        mMessagesView.showProgress();
        mMessagesInteractor.loadUserList(userList);
    }


    @Override
    public void onLoadUserListSucess() {
        mMessagesView.onLoadUserListSucess();
        mMessagesView.hideProgress();
    }

    @Override
    public void onLoadUserListEmpty() {
        mMessagesView.onLoadUserListEmpty();
    }

    @Override
    public void onLoadUserListFailure(ParseException e) {
        mMessagesView.hideProgress();
    }
}
