package com.rootcave.roadpal.main;

/**
 * Created by Lenovo T420 on 20.11.2015..
 */
public class MainPresenterImpl implements IMainPresenter,IonCheckUserLoginFinishedListener {
    private IMainView mainView;
    private IMainInteractor mainInteractor;

    public MainPresenterImpl(IMainView mainView) {
        this.mainView = mainView;
        this.mainInteractor=new MainInteractorImpl();
    }

//method from IMainPresenter
    @Override
    public void checkIfUserIsLoggedIn() {
    mainInteractor.checkUsersLogIn(this);
    }



//methods from IonCheckUserLoginFinishedListener
    @Override
    public void onUserAnonymus() {
        mainView.navigateToWelcomeActivity();

    }

    @Override
    public void onUserNotAnonymus() {
        mainView.navigateToMainFeedActivity();

    }


}
