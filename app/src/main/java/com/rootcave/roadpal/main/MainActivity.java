package com.rootcave.roadpal.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rootcave.roadpal.R;
import com.rootcave.roadpal.mainFeed.MainFeedActivity;
import com.rootcave.roadpal.welcome.WelcomeActivity;
import com.parse.ParseAnalytics;


/**
 * Importing appcompat and supportLibraries
 */

public class MainActivity extends AppCompatActivity implements IMainView {
    private IMainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        mainPresenter = new MainPresenterImpl(this);
         Thread giveTime = new Thread(){
            public void run(){
                //include inside the try and catch
                try{
                    sleep(2000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    mainPresenter.checkIfUserIsLoggedIn();
                }

            }
        };
        giveTime.start();




    }

    @Override
    public void onResume() {
        super.onResume();

       // mainPresenter.checkIfUserIsLoggedIn();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void navigateToMainFeedActivity() {

        startActivity(new Intent(MainActivity.this, MainFeedActivity.class));
        finish();
    }

    @Override
    public void navigateToWelcomeActivity() {
        startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
        finish();
    }
}