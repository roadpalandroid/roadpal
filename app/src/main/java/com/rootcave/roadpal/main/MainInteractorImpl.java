package com.rootcave.roadpal.main;

import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;

/**
 * Created by Lenovo T420 on 20.11.2015..
 */
public class MainInteractorImpl implements IMainInteractor {


    @Override
    public void checkUsersLogIn(IonCheckUserLoginFinishedListener listener) {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (ParseAnonymousUtils.isLinked(currentUser)) {
            // If user is anonymous, send the user to LoginSignupActivity.class
           listener.onUserAnonymus();
        } else {
            // If current user is NOT anonymous user
            if (currentUser != null) {
                // Send logged in users to MainFeedActivity.class
               listener.onUserNotAnonymus();

            } else {
                // Send user to LoginSignupActivity.class
                listener.onUserAnonymus();
            }
        }
    }


}
