package com.rootcave.roadpal.main;

/**
 * Created by Lenovo T420 on 20.11.2015..
 */
public interface IMainView {
    void navigateToMainFeedActivity();
    void navigateToWelcomeActivity();
}
