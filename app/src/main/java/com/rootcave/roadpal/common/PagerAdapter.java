package com.rootcave.roadpal.common;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 23.3.2016..
 */
public class PagerAdapter extends FragmentPagerAdapter{

    private FragmentManager mFragmentManager;
    private Context mContext;
    private ArrayList<Fragment> mFragmentList;


    public PagerAdapter(FragmentManager fragmentManager,Context context, ArrayList<Fragment> fragmentList) {
        super(fragmentManager);
        mContext= context;
        mFragmentManager=fragmentManager;
        mFragmentList=fragmentList;
    }

    @Override
    public Fragment getItem(int position) {

        //Log.d(RoadPalApplication.TAG,"Fragment name: "+mFragmentList.getClass().getSimpleName());
        Fragment fragment= mFragmentList.get(position);

        return fragment;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
