package com.rootcave.roadpal.common;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by Nikola on 24.4.2015..
 */
public class GenerateAppHashKey {

    private String hashKey;

    public GenerateAppHashKey(PackageManager manager) {
        getHashKey(manager);

    }

    private void getHashKey(PackageManager manager) {
        try {
            PackageInfo info = manager.getPackageInfo(
                    "com.example.sportssocialnetwork",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", "Name not found exception");
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", "no such algorithm exception");
        }
    }
}
