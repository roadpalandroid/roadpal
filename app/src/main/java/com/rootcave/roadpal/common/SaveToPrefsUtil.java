package com.rootcave.roadpal.common;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Lenovo T420 on 23.12.2015..
 */
public  class SaveToPrefsUtil {


    public static void saveToSharedPrefs(ParseUserModel user)
    {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(RoadPalApplication.getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(RoadPalApplication.PREFERENCE_PUSH_NOTIFICATION, (user.get_push_enabled()));
        editor.putString(RoadPalApplication.PREFERENCE_MOBILE_NUMBER,user.get_phone());
        editor.putString(RoadPalApplication.PREFERENCE_EMAIL, user.get_email());

        String vehicleType=user.get_vehicle_model();
        if(vehicleType==null || vehicleType.equals("") )
        {
            editor.putBoolean(RoadPalApplication.PREFERENCE_HAVE_VEHICLE, false);

        }
        else
        {
            editor.putBoolean(RoadPalApplication.PREFERENCE_HAVE_VEHICLE,true);
            editor.putString(RoadPalApplication.PREFERENCE_VEHICLE_MODEL, vehicleType);
            editor.putString(RoadPalApplication.PREFERENCE_VEHICLE_PLATES,user.get_vehicle_plates());

        }
        //  editor.putString(RoadPalApplication.PREFERENCE_PASSWORD, user.getString(user.getP));  password is not saved to preferences
        //  editor.commit();  sync save
        editor.apply(); //async save off the main thread



        /*
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(RoadPalApplication.getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(RoadPalApplication.PREFERENCE_PUSH_NOTIFICATION, (user.getBoolean(RoadPalApplication.KEY_PUSH)));
        editor.putString(RoadPalApplication.PREFERENCE_MOBILE_NUMBER,user.getString(RoadPalApplication.KEY_PHONE));
        editor.putString(RoadPalApplication.PREFERENCE_EMAIL, user.getEmail());

        String vehicleType=user.getString(RoadPalApplication.KEY_VEHICLE_MODEL);
        if(vehicleType==null || vehicleType.equals("") )
        {
            editor.putBoolean(RoadPalApplication.PREFERENCE_HAVE_VEHICLE, false);

        }
        else
        {
            editor.putBoolean(RoadPalApplication.PREFERENCE_HAVE_VEHICLE,true);
            editor.putString(RoadPalApplication.PREFERENCE_VEHICLE_MODEL, vehicleType);
            editor.putString(RoadPalApplication.PREFERENCE_VEHICLE_PLATES,user.getString(RoadPalApplication.KEY_VEHICLE_PLATES));

        }
        //  editor.putString(RoadPalApplication.PREFERENCE_PASSWORD, user.getString(user.getP));  password is not saved to preferences
        //  editor.commit();  sync save
        editor.apply(); //async save off the main thread
*/
    }

}
