package com.rootcave.roadpal.common;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

/**
 * Created by Lenovo T420 on 25.12.2015..
 */
//@ParseClassName("_User")
public class ParseUserModel /*extends ParseUser */ {

    private ParseUser user;
    private String password;

    public ParseUserModel(ParseUser user) {
        this.user = user;
    }

    public ParseUser getUser() {return user;}
    public String get_object_id() {
        return user.getObjectId();
    }
    public String get_username() {
        return user.getUsername();
    }

    //cannot fetch password from server due to data protection
    public String getPassword() {
        return password;
    }
    public String get_email() {
        return user.getEmail();
    }
    public String get_full_name() {
        return user.getString("fullname");
    }
    public String get_gender() {
        return user.getString("gender");
    }
    public String get_phone() {
        return user.getString("phone");
    }
    public String get_dob() {
        return user.getString("dob");
    }

    //TODO  perhaps implement retrieving of users_image from here
    public ParseFile get_users_image() {
        return user.getParseFile("users_image");
    }
    public Boolean get_email_verified() {
        return user.getBoolean("emailVerified");
    }
    public Boolean get_push_enabled() {
        return user.getBoolean("pushEnabled");
    }
    public String get_vehicle_model() {
        return user.getString("vehicleModel");
    }
    public String get_vehicle_plates() {return user.getString("vehiclePlates");}
    public boolean isOnline(){return user.getBoolean("isOnline");}
    public boolean hasVehicle(){
        return (!get_vehicle_model().isEmpty());

        //TODO check if null pointer can happen
}



    public void set_object_id(String objectId) {
        user.put("objectId", objectId);
    }
    public void set_username(String username) {user.setUsername(username);}
    public void set_password(String password) {
        this.password = password;
        user.setPassword(password);
    }

    public void set_email(String email) {
        user.setEmail(email);
    }
    public void set_fullname(String fullname) {
        user.put("fullname", fullname);
    }
    public void set_gender(String gender) {
        user.put("gender", gender);
    }
    public void set_phone(String phone) {
        user.put("phone", phone);
    }
    public void set_dob(String dob) {
        user.put("dob", dob);
    }

    //TODO  perhaps implement saving of users_image from here
    public void set_users_image(ParseFile users_image) {
        user.put("users_image", users_image);
    }
    public void set_email_verified(Boolean emailVerified) {
        user.put("emailVerified", emailVerified);
    }

    public void set_push_enabled(Boolean pushEnabled) {
        user.put("pushEnabled", pushEnabled);
    }
    public void set_vehicle_model(String vehicleModel) {user.put("vehicleModel", vehicleModel);}
    public void set_vehicle_plates(String vehiclePlates) {user.put("vehiclePlates", vehiclePlates);}
    public void setOnlineStatus(boolean isOnline){user.put("isOnline",isOnline);}
    public boolean has_Users_image() {
        return user.has("users_image");
    }
    public boolean is_users_imageAvailable() {return user.getParseFile("users_image").isDataAvailable();}
    public void save_user_in_background(SaveCallback callback) {
        user.saveInBackground(callback);
    }
    public void save_user_eventually(SaveCallback callback){user.saveEventually(callback);}
    public void save_user() throws ParseException {user.save();}

    public void login_in_background(String username, String password, LogInCallback callback) {
        user.logInInBackground(username, password, callback);
    }

    public void sign_up_in_background(SignUpCallback callback) {
        user.signUpInBackground(callback);

    }

}
