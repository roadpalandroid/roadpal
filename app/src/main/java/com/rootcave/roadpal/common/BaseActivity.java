package com.rootcave.roadpal.common;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.rootcave.roadpal.R;
import com.rootcave.roadpal.message.InboxActivity;
import com.rootcave.roadpal.ride.createRide.CreateRideActivity;
import com.rootcave.roadpal.ride.findRide.FindRideActivity;
import com.rootcave.roadpal.mainFeed.MainFeedActivity;
import com.rootcave.roadpal.ride.myRides.MyRidesActivity;
import com.rootcave.roadpal.profile.ProfileActivity;
import com.rootcave.roadpal.settings.SettingsActivity;
import com.rootcave.roadpal.welcome.WelcomeActivity;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.parse.GetFileCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 26.11.2015..
 */
public abstract class BaseActivity extends LifecycleLoggingActivity {
    //TODO refactor by implementing abstract methods  http://www.tutorialspoint.com/java/java_abstraction.htm
    private static final String TAG_HOME = "home";
    private static final String TAG_FIND_RIDE = "find ride";
    private static final String TAG_CREATE_RIDE = "create ride";
    private static final String TAG_MY_RIDES = "my rides";
    private static final String TAG_PROFILE = "profile";
    private static final String TAG_SETTINGS = "my settings";
    private static final String TAG_INBOX = "inbox";


    private Toolbar toolbar;
    private Drawer mDrawer;
    private AccountHeader headerResult;
    private ParseUserModel currentUser;
    private Drawable usersImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(RoadPalApplication.TAG, "oncreate base class");
        // onCreateNavDrawer();
        //  setContentView(mDrawer.getDrawerLayout());

    }

    protected void onCreateToolbar() {
        //Locate Toolbar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        onCreateNavDrawer();
    }

    protected void onCreateNavDrawer() {

        currentUser = new ParseUserModel(ParseUser.getCurrentUser());
        //currentUser.setUser(ParseUser.getCurrentUser());
        // (ParseUserModel)ParseUser.getCurrentUser();


        usersImage = getResources().getDrawable(R.drawable.ic_people);

        if (currentUser.has_Users_image()) {
            Log.d(RoadPalApplication.TAG, "currentUser.is_users_imageAvailable(): " + currentUser.is_users_imageAvailable());
            if (currentUser.is_users_imageAvailable()) {
                Log.d(RoadPalApplication.TAG, "currentUser.get_users_image(): " + currentUser.get_users_image().toString());
                ParseFile file = currentUser.get_users_image();
                //on first run it fetches file from network, after that it gets the file from cache
                file.getFileInBackground(new GetFileCallback() {
                    @Override
                    public void done(File file, ParseException e) {
                        if (e == null) {
                            //  Log.d(RoadPalApplication.TAG, " file.getAbsolutePath(): " +  file.getAbsolutePath());
                            usersImage = Drawable.createFromPath(file.getAbsolutePath());
                            Log.d(RoadPalApplication.TAG, "file.getAbsolutePath() :" + file.getAbsolutePath());
                            headerResult.removeProfile(0);
                            headerResult.addProfiles(new ProfileDrawerItem().withName(currentUser.get_username()).withEmail(currentUser.get_email()).withIcon(usersImage));
                        }
                    }
                });
            }
        }

        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.background)
                .addProfiles(
                        new ProfileDrawerItem().withName(currentUser.get_full_name())
                                .withEmail(currentUser.get_email())
                                        // .withIcon("https://scontent.xx.fbcdn.net/hprofile-xfp1/v/t1.0-1/p200x200/11222685_10207850949693083_6752560282101619875_n.jpg?oh=f9166ffe374ced2443eefffa18a7a7b6&oe=56D91B21")
                                .withIcon(usersImage)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        Log.d(RoadPalApplication.TAG, "profile changed : " + profile.toString());
                        return false;
                    }
                })
                .build();

        //create the drawer and remember the `Drawer` result object
        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .build();

        addDrawerItems();
        addDrawerItemCLickListener();

    }

    private void addDrawerItems() {
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName(R.string.drawer_item_home).withIcon(GoogleMaterial.Icon.gmd_home).withTag(TAG_HOME);
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withName(R.string.drawer_item_find_ride).withIcon(GoogleMaterial.Icon.gmd_find_in_page).withTag(TAG_FIND_RIDE);
        SecondaryDrawerItem item3 = new SecondaryDrawerItem().withName(R.string.drawer_item_create_rides).withIcon(GoogleMaterial.Icon.gmd_search).withTag(TAG_CREATE_RIDE);
        SecondaryDrawerItem item4 = new SecondaryDrawerItem().withName(R.string.drawer_item_my_rides).withIcon(GoogleMaterial.Icon.gmd_settings).withTag(TAG_MY_RIDES);
        SecondaryDrawerItem item5 = new SecondaryDrawerItem().withName(R.string.drawer_item_profile).withIcon(GoogleMaterial.Icon.gmd_settings).withTag(TAG_PROFILE);
        SecondaryDrawerItem item6 = new SecondaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(GoogleMaterial.Icon.gmd_settings).withTag(TAG_SETTINGS);
        //        Inbox
        SecondaryDrawerItem item7 = new SecondaryDrawerItem().withName(R.string.drawer_item_message).withIcon(GoogleMaterial.Icon.gmd_message).withBadge("19").withBadgeStyle(new BadgeStyle()).withTag(TAG_INBOX);

        IDrawerItem[] list = null;
        if (currentUser.hasVehicle()) {
            list = new IDrawerItem[]{item1, new DividerDrawerItem(), item2, item3, item4, item5, item6, item7};
        } else {
            list = new IDrawerItem[]{item1, new DividerDrawerItem(), item2, item4, item5, item6, item7};
        }
        mDrawer.addItems(list);

    }
    private void addDrawerItemCLickListener() {

        mDrawer.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                // do something with the clicked item :D
                switch ((String) drawerItem.getTag()) {
                    case TAG_HOME:
                        openActivity(getBaseContext(), MainFeedActivity.class);
                        break;

                    case TAG_FIND_RIDE:
                        openActivity(getBaseContext(), FindRideActivity.class);
                        break;

                    case TAG_CREATE_RIDE:

                        openActivity(getBaseContext(), CreateRideActivity.class);
                        break;

                    case TAG_MY_RIDES:
                        Log.d(RoadPalApplication.TAG, "My rides  case!");
                        openActivity(getBaseContext(), MyRidesActivity.class);
                        break;

                    case TAG_PROFILE:
                        openActivity(getBaseContext(), ProfileActivity.class);
                        break;

                    case TAG_SETTINGS:
                        openActivity(getBaseContext(), SettingsActivity.class);
                        break;

                    case TAG_INBOX:
                        openActivity(getBaseContext(), InboxActivity.class);
                        break;
                }

                mDrawer.closeDrawer();

                return true;
            }
        });


    }

    private void openActivity(Context context, Class activity) {
        Intent intent = new Intent(
                context,
                activity);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:

                break;
            case R.id.action_disclaimer:
                FragmentManager manager = getSupportFragmentManager();
                DisclaimerDialogFragment dialogFragment = new DisclaimerDialogFragment();
                dialogFragment.show(manager, "MyDialog");
                break;
            case R.id.action_logout:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                //needed to unregister push after logout
                                RoadPalApplication.updateParseInstallationLogout();

                                startActivity(new Intent(BaseActivity.this, WelcomeActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
        }
      /*  if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        //   mDrawerToggle.onConfigurationChanged(newConfig);
    }


    /**
     * Sets the touch and click listener for a view with given id.
     *
     * @param id
     *            the id
     * @return the view on which listeners applied
     */


}
