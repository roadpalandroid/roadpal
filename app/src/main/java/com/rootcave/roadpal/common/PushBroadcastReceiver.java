package com.rootcave.roadpal.common;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.rootcave.roadpal.ride.myRides.MyRidesActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lenovo T420 on 4.2.2016..
 */
public class PushBroadcastReceiver extends ParsePushBroadcastReceiver {
    @Override
    protected void onPushOpen(Context context, Intent intent) {
        //Emulator needs to have gapps installed in order for push to work
        Log.d(RoadPalApplication.TAG, "PushBroadcastReceiver Push clicked");
        Log.d(RoadPalApplication.TAG, "PushBroadcastReceiver Push message:" + intent.getExtras().getString("com.parse.Data"));
        String sender = null;
        try {
            JSONObject jsonObject = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            sender = jsonObject.getString("sender");
        } catch (JSONException e) {
            Log.e(RoadPalApplication.TAG, "Push message json exception: " + e.getMessage());
            e.printStackTrace();
        }

        if (sender.equals("PendingNotification")) {
            Intent i = new Intent(context, MyRidesActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } else if (sender.equals("AcceptPassengerNotification")) {

            Log.d(RoadPalApplication.TAG, "AcceptPassengerNotification Push received");
        }

    }
}
