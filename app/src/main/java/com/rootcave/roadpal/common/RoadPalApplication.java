package com.rootcave.roadpal.common;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.parse.ParseConfig;
import com.rootcave.roadpal.profile.RideRatingModel;
import com.rootcave.roadpal.ride.RideModel;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.interceptors.ParseLogInterceptor;
import com.rootcave.roadpal.ride.createRide.LocationPointModel;

public class RoadPalApplication extends Application {
    public static final String TAG = "Road pal app";
    public static final String KEY_USER_ID = "userId";


    //Parse Road_Pal
    public static final String PARSE_APPLICATION_ID = "myAppId";
    public static final String PARSE_CLIENT_ID = "myMasterKey";
    public static final String SERVER = "http://192.168.56.1:1337/parse/";


    public static final String GOOGLE_API_KEY = "AIzaSyDcccr1CnPnbhg_u399pOw05t7xQKx0r70";    // Server Api Key, others dont work, need to enable google Places web service in developers api console

    public static final String PREFERENCE_PUSH_NOTIFICATION = "push_notification_preference";
    public static final String PREFERENCE_THEME = "theme_preference";
    public static final String PREFERENCE_MOBILE_NUMBER = "mobile_number_preference";
    public static final String PREFERENCE_EMAIL = "email_preference";
    public static final String PREFERENCE_HAVE_VEHICLE = "parent_vehicle_preference";
    public static final String PREFERENCE_VEHICLE_MODEL = "vehicle_model_preference";
    public static final String PREFERENCE_VEHICLE_PLATES = "vehicle_plates_preference";
    public static final String PREFERENCE_PASSWORD = "password_preference";   // just for reference, password is not saved to prefs
    // public static final String PARENT_VEHICLE_PREFERENCE="parent_vehicle_preference";

    public static final String KEY_PUSH = "pushEnabled";
    public static final String KEY_FULLNAME = "fullname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_DOB = "dob";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_USERS_IMAGE = "users_image";
    public static final String KEY_VEHICLE_MODEL = "vehicleModel";
    public static final String KEY_VEHICLE_PLATES = "vehiclePlates";

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

       // Stetho.initializeWithDefaults(this);
       // Parse.addParseNetworkInterceptor(new ParseStethoInterceptor());
        // sending all network calls to logcat
      //  Parse.addParseNetworkInterceptor(new ParseLogInterceptor());


      //  Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(RideModel.class);
        ParseUser.registerSubclass(LocationPointModel.class);
        ParseUser.registerSubclass(RideRatingModel.class);





        //Parse.initialize(this, PARSE_APPLICATION_ID, PARSE_CLIENT_ID);
        Parse.initialize(new Parse.Configuration.Builder(this).addNetworkInterceptor(new ParseLogInterceptor()).enableLocalDataStore().server(SERVER).applicationId(PARSE_APPLICATION_ID).clientKey(PARSE_CLIENT_ID).build());

        //ParseInstallation.getCurrentInstallation().saveInBackground();  // used to register app for push

        ParseUser.enableAutomaticUser();
        //   ParseACL defaultACL = new ParseACL();

        FacebookSdk.sdkInitialize(this);
        ParseFacebookUtils.initialize(this);
    }



    public static void updateParseInstallation(ParseUser user) {
        Log.d(RoadPalApplication.TAG, "Parse installation updated with UserId: " + user.getObjectId());
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(KEY_USER_ID, user.getObjectId());
        //TODO  implement on no network error handling
        installation.saveInBackground();


    }

    public static void updateParseInstallationLogout() {
        Log.d(RoadPalApplication.TAG, "Parse installation logout! " );
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(KEY_USER_ID, "loggedOut");
        //TODO  implement on no network error handling
        installation.saveInBackground();
    }

    public static Context getContext() {
        return mContext;
    }

    public static ParseUserModel getCurrentUser() {
        ParseUserModel currentUser = new ParseUserModel(ParseUser.getCurrentUser());

        return currentUser;
    }
}
