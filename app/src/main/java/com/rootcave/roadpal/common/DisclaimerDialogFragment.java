package com.rootcave.roadpal.common;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rootcave.roadpal.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class DisclaimerDialogFragment extends DialogFragment {
Button bDisclaimerOk;

    public DisclaimerDialogFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //dialoge won't be cancled if you pressed outside the dialog
        setCancelable(false);
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_disclaimer_dialog, null);
        bDisclaimerOk=(Button)view.findViewById(R.id.bDisclaimerOK);
        bDisclaimerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }



    }


