# README #

This is the Android Version of the Car Pooling App named Road Pal.

### What is this repository for? ###

* Android Developer Team of Road Pal
* Version
* Team Work Development Integration With Slack and Trello

### How do I get set up? ###


* Clone the Repo
* Dependency Configuration in Gradle inside Android Studio and Build
* Create a Parse Account and Join the App on Parse
* Test the App on Lollipop Devices + , Also Test it on Lower SDK Versions (Froyo)
* Use Genymotion as your Testing Platform , or a Real Device



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* bitbucket: @melkotoury
* github   : https://github.com/melkotoury
* facebook : https://facebook.com/melkotoury
* twitter  : https://twitter.com/melkotoury